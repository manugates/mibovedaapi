### Prerrequisitos

Asegúrese que cumple con las siguientes afirmaciones antes de enviar la solicitud.

- [ ] Confirmé que estoy informando el problema en el repositorio correcto.
- [ ] Confirmé que la solicitud no existe.

### Comportamiento actual

Describa de forma clara y precisa el comportamiento actual

### Pasos para reproducir el comportamiento actual

Por favor describa en detalle el comportamiento esperado. Si es necesario incluya toda la documentación que ayude a resolver el problema, si son documentos existentes en el proyecto, indicar el nombre del archivo de la carpeta /doc

### Comportamiento esperado

Por favor describa el comportamiento esperado como resultado, que debería realizar esta característica o funcionalidad una vez implementada. 
Si existe un requerimiento funcional o requerimiento no funcional, por favor indicar el números de referencia asociados a esta funcionalidad.

/label ~feature-edit