### Prerrequisitos

Asegúrese que cumple con las siguientes afirmaciones antes de enviar la solicitud.

- [ ] Confirmé que estoy informando el problema en el repositorio correcto.
- [ ] Confirmé que el documento existe en el repositorio.
- [ ] Confirmé que la solicitud no existe.

### Seleccione el documento a modificar.

- [ ] DOC_Checklist_Instalacion
- [ ] DOC_Diseño
- [ ] DOC_Esp_Requerimientos
- [ ] MAN_Instalacion
- [ ] MAN_Sistema
- [ ] MAN_Usuario
- [ ] MAN_Operaciones

### Cambios a realizar

Describa de forma clara y precisa el cambio requerido, indicando el motivo de los cambios y cual es el resultado esperado.

/label ~doc-edit