## Prerrequisitos

Asegúrese que cumple con las siguientes afirmaciones antes de enviar la solicitud.

- [ ] Confirmé que estoy creando la solicitud en el repositorio correcto.
- [ ] Confirmé que la solicitud no existe.

## Datos de la reunión

*  *Fecha y Hora*:  
*  *Lugar*: 
*  *Motivo de la solicitud*: (debe describir la razón por la que requiere de apoyo técnico, por ejemplo: El cliente requiere reunión para entender arquitectura de proyecto).
*  *Otros* (debe detallar toda la información relevante para la reunión):

/label ~meeting