### Prerrequisitos

Asegúrese que cumple con las siguientes afirmaciones antes de enviar la solicitud.

- [ ] Confirmé que estoy informando el problema en el repositorio correcto.
- [ ] Confirmé que el documento no existe en el repositorio.
- [ ] Confirmé que la solicitud no existe.

### Seleccione el documento que se debe crear.

- [ ] DOC_Checklist_Instalacion
- [ ] DOC_Diseño
- [ ] DOC_Esp_Requerimientos
- [ ] MAN_Instalacion
- [ ] MAN_Sistema
- [ ] MAN_Usuario
- [ ] MAN_Operaciones

/label ~doc-new