## Prerrequisitos

Asegúrese que cumple con las siguientes afirmaciones antes de enviar la solicitud.

- [ ] Confirmé que estoy creando la solicitud en el repositorio correcto.
- [ ] Confirmé que la solicitud no existe.

## Datos de la reunión

* *Adjuntar documentación técnica que detalle aspectos relevantes para esta solicitud*:
* *Indicar Pasos requeridos*:  
   - [ ] Compilación
   - [ ] Pruebas Unitarias Automáticas
   - [ ] SonarQube
   - [ ] Despliegue de "Puesta en Escena" utilizando el sistema de archivos. 
   - [ ] Despliegue de "Puesta en Escena" utilizando Docker.
   - [ ] Despliegue en "Producción" utilizando el sistema de archivos.
   - [ ] Despliegue en "Producción" utilizando Docker.
* *Si seleccionó el Despliegue utilizando sistema de archivos debe indicar*:
   * Server IP:
   * Folder:

/label ~devops