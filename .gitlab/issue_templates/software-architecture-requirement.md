## Prerrequisitos

Asegúrese que cumple con las siguientes afirmaciones antes de enviar la solicitud.

- [ ] Confirmé que estoy creando la solicitud en el repositorio correcto.
- [ ] Confirmé que la solicitud no existe.

## Datos de la reunión

* *URL al documento DOC_Esp_Requerimientos.doc del proyecto en gitlab*: 
 
/label ~software-architecture