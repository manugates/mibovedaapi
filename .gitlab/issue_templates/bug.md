### Prerrequisitos

Asegúrese que cumple con las siguientes afirmaciones antes de enviar la solicitud.

- [ ] Confirmé que estoy informando el problema en el repositorio correcto.
- [ ] Confirmé que la solicitud no existe.
- [ ] Confirmé que el problema no sea de datos o un error en otra aplicación.

### Comportamiento actual

Describa de forma clara y precisa el comportamiento actual

### Pasos para reproducir

Describa de forma clara y precisa los pasos a seguir para reproducir el problema en forma enumerada.

### Comportamiento esperado

Por favor describa en detalle el comportamiento esperado. Si es necesario incluya toda la documentación que ayude a resolver el problema, si son documentos existentes en el proyecto, indicar el nombre del archivo de la carpeta /doc

### Contexto

Por favor, proporcione cualquier información relevante sobre su configuración. Esto es importante en caso de que el problema no sea reproducible.

*  *Sistema operativo*:
*  *Navegador web*:
*  *Resolución de pantalla*:

### Registros de fallos

Por favor incluya cualquier registro o archivo de logs asociado a este error.

/label ~bug