### Descripción

Incluya un resumen del cambio y el problema que se ha solucionado. Por favor, incluya también la motivación relevante y el contexto. Listar las dependencias que se requieren para este cambio.

### Tipo de cambio

Por favor elimine las opciones que no son relevantes para el caso.

- [ ] Arreglo de error.
- [ ] Nueva característica.
- [ ] Modificación de característica existente.
- [ ] Nueva Documentación.
- [ ] Actualización de Documentación

### Cómo se ha probado?

Describa las pruebas que realizó para verificar sus cambios. Proporcione las instrucciones necesarias para su reproducción. Enumere cualquier detalle relevante para la configuración de la prueba.

**Configuración de prueba (Si es que aplica)**:

* Hardware:
* SDK:

### Checklist:

- [ ] Mi código sigue las pautas de estilo de este proyecto.
- [ ] He realizado una autoevaluación de mi propio código.
- [ ] He realizado los cambios correspondientes a la documentación producto de los cambios.
- [ ] Mis cambios no generan nuevas advertencias al compilar.
- [ ] He agregado prebas unitarias para confirmar que mi corrección es efectiva o que mi función funciona
- [ ] Las pruebas unitarias nuevas y existentes pasan localmente con mis cambios

/label ~merge-request