﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class InformacionIsapre
    {
        public decimal IdInformacionIsapre { get; set; }
        public decimal IdTipoIsapre { get; set; }
        public decimal IdUsuario { get; set; }
        public decimal IdIsapre { get; set; }
    }
}
