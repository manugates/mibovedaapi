﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Archivo
    {
        public decimal IdArchivo { get; set; }
        public decimal File { get; set; }
        public decimal IdUsuario { get; set; }
        public decimal IdTipoArchivo { get; set; }

    }
}
