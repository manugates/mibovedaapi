﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Cuenta
    {
        public decimal IdCuenta { get; set; }
        public string Numero { get; set; }
        public decimal IdBanco { get; set; }
        public decimal IdTipoCuenta { get; set; }
        public decimal IdUsuario { get; set; }
    }
}
