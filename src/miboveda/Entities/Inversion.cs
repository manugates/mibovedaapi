﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Inversion
    {
        public decimal IdInversion { get; set; }
        public string Descripcion { get; set; }
        public decimal IdTipoInversion { get; set; }

        public decimal IdBanco { get; set; }
        public decimal IdUsuario { get; set; }
    }
}
