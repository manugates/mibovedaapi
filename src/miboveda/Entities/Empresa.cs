﻿using System;


namespace miboveda.Entities
{
    public class Empresa
    {
        public decimal IdEmpresa { get; set; }
        public string Nombre { get; set; }
        public string RazonSocial { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string Direccion { get; set; }
        public string Cargo { get; set; }
        public string Profesion { get; set; }
        public decimal IdCajaCompensacion { get; set; }
        public string NombreContacto { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Rut { get; set; }
        public decimal Comuna { get; set; }
        public decimal IdUsuario { get; set; }
    }
}
