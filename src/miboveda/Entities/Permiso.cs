﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Permiso
    {
        public decimal Seguro { get; set; }
        public decimal CuentaBancaria { get; set; }
        public decimal DatosProvisionales { get; set; }
        public decimal Bienes { get; set; }
        public decimal Deudas { get; set; }
        public decimal FotosVideos { get; set; }
        public decimal Documentos { get; set; }
        public decimal Deseos { get; set; }
        public decimal Instrucciones { get; set; }
        public decimal Testimonios { get; set; }
        public decimal IdContacto { get; set; }
     
    }
}
