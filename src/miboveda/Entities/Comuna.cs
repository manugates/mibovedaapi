﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Comuna
    {
        public decimal IdComuna { get; set; }
        public string Nombre { get; set; }
    }
}
