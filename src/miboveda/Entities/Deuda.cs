﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Deuda
    {
        public decimal IdDeuda { get; set; }
        public string Operacion { get; set; }
        public decimal Monto { get; set; }
        public string SeguroDegravament { get; set; }
        public string Mora { get; set; }
        public decimal IdUsuario { get; set; }
        public decimal IdTipoDeuda { get; set; }
        public decimal IdBanco { get; set; }
        public decimal IdProducto { get; set; }
        public decimal IdCaja { get; set; }
        public decimal IdCasaComercial { get; set; }
        public decimal IdEntidad { get; set; }

    }
}
