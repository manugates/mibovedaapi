﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Seguro
    {
        public decimal IdSeguro { get; set; }
        public string NumeroPoliza { get; set; }
        public decimal IdTipoSeguro { get; set; }
        public decimal IdAseguradora { get; set; }
        public decimal IdUsuario { get; set; }
        public DateTime Vigencia { get; set; }
        public string Ahorro { get; set; }
    }
}
