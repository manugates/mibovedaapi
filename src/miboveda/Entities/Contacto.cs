﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Contacto
    {
        public decimal IdContacto { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Rut { get; set; }
        public string Password { get; set; }
        public DateTime FechaInscripcion { get; set; }
        public string Email { get; set; }
        public decimal IdUsuario { get; set; }
        public decimal IdParentesco { get; set; }
        public string Sexo { get; set; }

    }
}
