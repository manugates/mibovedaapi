﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Bienes
    {
        public decimal IdBien { get; set; }
        public string Descripcion { get; set; }
        public decimal IdUsuario { get; set; }
        public decimal IdTipoBien { get; set; }
        public decimal IdTipoDominio { get; set; }
        public decimal IdTipoInmueble { get; set; }
        public decimal IdTipoMueble { get; set; }
    }
}
