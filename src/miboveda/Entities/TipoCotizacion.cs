﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class TipoCotizacion
    {
        public decimal IdTipoCotizacion { get; set; }
        public string Descripcion { get; set; }
    }
}
