﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class CasaComercial
    {
        public decimal IdCasaComercial { get; set; }
        public string Descripcion { get; set; }
    }
}
