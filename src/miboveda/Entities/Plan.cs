﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Plan
    {
        public decimal IdPlan { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
    }
}
