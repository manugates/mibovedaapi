﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class InformacionAfp
    {
        public decimal IdInformacionAfp { get; set; }
        public decimal IdTipoFondo { get; set; }
        public decimal IdUsuario { get; set; }
        public decimal IdAfp { get; set; }
        public decimal IdTipoCotizacion { get; set; }
    }
}
