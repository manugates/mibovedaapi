﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Parentesco
    {
        public decimal IdParentesco { get; set; }
        public string Descripcion { get; set; }
    }
}
