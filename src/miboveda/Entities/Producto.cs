﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Producto
    {
        public decimal IdProducto { get; set; }
        public string Descripcion { get; set; }
    }
}
