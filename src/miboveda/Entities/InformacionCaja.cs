﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class InformacionCaja
    {
        public decimal IdInformacionCaja { get; set; }
        public decimal IdUsuario { get; set; }
        public decimal IdCajaCompensacion { get; set; }
    }
}
