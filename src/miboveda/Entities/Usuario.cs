﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Usuario
    {
        public decimal IdUsuario { get; set; }
        public string Rut { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public DateTime FechaInscripcion { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaUpdate { get; set; }
        public decimal CreadoPor { get; set; }
        public decimal IdAfp { get; set; }
        public decimal IdIsapre { get; set; }
        public decimal IdPlan { get; set; }
        public string Telefono { get; set; }
        public string Comuna { get; set; }
        
    }
}
