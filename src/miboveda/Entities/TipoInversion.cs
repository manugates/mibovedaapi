﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class TipoInversion
    {
        public decimal IdTipoInversion { get; set; }
        public string Descripcion { get; set; }
    }
}
