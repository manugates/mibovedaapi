﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Entities
{
    public class Isapre
    {
        public decimal IdIsapre { get; set; }
        public string Nombre { get; set; }
    }
}
