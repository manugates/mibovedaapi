﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Archivo
{
    public class Update
    {
        public class Input
        {
            public decimal IdArchivo { get; set; }
            public decimal File { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdTipoArchivo { get; set; }
        }

        public class Output
        {
            public decimal IdArchivo { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var ArchivoExist = await _context.Archivos.AnyAsync(cancellationToken);
                if (!ArchivoExist)
                {
                    
                }

                Entities.Archivo archivo = _mapper.Map<Entities.Archivo>(request.Input);
                _context.Update(archivo);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdArchivo = archivo.IdArchivo });
            }
        }
    }
}
