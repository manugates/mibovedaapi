﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Archivo
{
    public class GetById
    {
        public class Input
        {
            public decimal IdArchivo { get; set; }
        }

        public class Output
        {
            public decimal IdArchivo { get; set; }
            public decimal File { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdTipoArchivo { get; set; }
        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from Archivo in _context.Archivos
                                    where Archivo.IdArchivo == request.Input.IdArchivo
                                    select new Output
                                    {
                                        IdArchivo = Archivo.IdArchivo,
                                        File = Archivo.File,
                                        IdUsuario = Archivo.IdUsuario,
                                        IdTipoArchivo = Archivo.IdTipoArchivo


                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
