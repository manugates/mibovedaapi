﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Archivo
{
    public class Delete
    {
        public class Input
        {
            public decimal File { get; set; }

        }

        public class Outut
        {
            public decimal IdArchivo { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var archivo = await _context.Archivos
                    .Where(x => x.File == request.Input.File)
                    .ToListAsync(cancellationToken);

                if (archivo.Count() == 0)
                {
                    
                }

                var archivoDelete = archivo.FirstOrDefault();

                _context.Archivos.Remove(archivoDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdArchivo = archivoDelete.IdArchivo;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
