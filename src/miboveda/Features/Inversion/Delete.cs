﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Inversion
{
    public class Delete
    {
        public class Input
        {
            public decimal IdInversion { get; set; }

        }

        public class Outut
        {
            public decimal IdInversion { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var inversion = await _context.Inversiones
                    .Where(x => x.IdInversion == request.Input.IdInversion)
                    .ToListAsync(cancellationToken);

                if (inversion.Count() == 0)
                {
                   
                }

                var inversionDelete = inversion.FirstOrDefault();

                _context.Inversiones.Remove(inversionDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdInversion = inversionDelete.IdInversion;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
