﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Inversion
{
    public class InversionExiste
    {
        public class Input
        {
            public string Descripcion { get; set; }
            public decimal IdTipoInversion { get; set; }
            public decimal IdBanco { get; set; }
        }

        public class Output
        {
            public decimal IdInversion { get; set; }
            public string Descripcion { get; set; }
            public string IdTipoInversion { get; set; }
            public string IdBanco { get; set; }
            public decimal Idusuario { get; set; }
        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from Inversion in _context.Inversiones
                                    join banco in _context.Bancos on Inversion.IdBanco equals banco.IdBanco
                                    join TipoInversion in _context.TipoInversiones on Inversion.IdTipoInversion equals TipoInversion.IdTipoInversion
                                    where Inversion.Descripcion == request.Input.Descripcion && Inversion.IdTipoInversion == request.Input.IdTipoInversion && Inversion.IdBanco == request.Input.IdBanco
                                    select new Output
                                    {
                                        IdInversion = Inversion.IdInversion,
                                        Descripcion = Inversion.Descripcion,
                                        IdTipoInversion = TipoInversion.Descripcion,
                                        IdBanco = banco.Nombre,
                                        Idusuario = Inversion.IdUsuario


                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
