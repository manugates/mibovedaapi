﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Inversion
{
    public class Update
    {
        public class Input
        {
            public decimal IdInversion { get; set; }
            public string Descripcion { get; set; }
            public decimal IdTipoInversion { get; set; }
            public decimal IdBanco { get; set; }
            public decimal IdUsuario { get; set; }
        }

        public class Output
        {
            public decimal IdInversion { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var InversionExist = await _context.Inversiones.AnyAsync(cancellationToken);
                if (!InversionExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Descripcion);
                }

                Entities.Inversion inversion = _mapper.Map<Entities.Inversion>(request.Input);
                _context.Update(inversion);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdInversion = inversion.IdInversion });
            }
        }
    }
}
