﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Constants;
using miboveda.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace miboveda.Features.Inversion
{
    public class Create
    {
        public class Input
        {
            public decimal IdInversion { get; set; }
            public string Descripcion { get; set; }
            public decimal IdTipoInversion { get; set; }
            public decimal IdBanco { get; set; }
            public decimal IdUsuario { get; set; }
        }

        public class Output
        {
            public decimal IdInversion { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {

        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public decimal GetMySequence()
            {
                var stringSeq = Database.Sequences.InversionSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {

                decimal code = 3;
                request.Input.IdInversion = GetMySequence();
                var inversionCreate = _mapper.Map<Entities.Inversion>(request.Input);

                await _context.Inversiones.AddAsync(inversionCreate);

                _context.SaveChanges();

                code = inversionCreate.IdInversion;

                return code == -1
                    ? RequestResult<Output>.Fail("La inversion no se pudo crear")
                    : RequestResult<Output>.Success(new Output { IdInversion = inversionCreate.IdInversion });
            }
        }
    }
}
