﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using miboveda.Features.Shared;

namespace miboveda.Features.TipoInmueble
{
    [Route("api/TipoInmueble")]
    public class TipoInmuebleController : BaseController
    {
        public TipoInmuebleController(IMediator mediator) : base(mediator)
        {

        }

        [HttpGet]
        [Route("getList")]
        [ProducesResponseType(typeof(GetList.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            var request = new GetList.Request();
            return await HandleRequestAsync(request);
        }
    }
}
