﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Seguro
{
    public class PolizaExiste
    {
        public class Input
        {
            public string NumeroPoliza { get; set; }
            public decimal IdTipoSeguro { get; set; }
            public decimal IdAseguradora { get; set; }
        }

        public class Output
        {
            public decimal IdSeguro { get; set; }
            public string NumeroPoliza { get; set; }
            public string IdTipoSeguro { get; set; }
            public string IdAseguradora { get; set; }
            public decimal IdUsuario { get; set; }
            public string Vigencia { get; set; }
            public string Ahorro { get; set; }
        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from Seguro in _context.Seguros
                                    join tipoSeguro in _context.TipoSeguros on Seguro.IdTipoSeguro equals tipoSeguro.IdTipoSeguro
                                    join aseguradora in _context.Aseguradoras on Seguro.IdAseguradora equals aseguradora.IdAseguradora
                                    where Seguro.NumeroPoliza == request.Input.NumeroPoliza && Seguro.IdTipoSeguro == request.Input.IdTipoSeguro && Seguro.IdAseguradora == request.Input.IdAseguradora

                                    select new Output
                                    {
                                        IdSeguro = Seguro.IdSeguro,
                                        NumeroPoliza = Seguro.NumeroPoliza,
                                        IdTipoSeguro = tipoSeguro.Descripcion,
                                        IdAseguradora = aseguradora.Descripcion,
                                        IdUsuario = Seguro.IdUsuario,
                                        Vigencia = Seguro.Vigencia.ToString("dd-MM-yyyy"),
                                        Ahorro = Seguro.Ahorro


                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
