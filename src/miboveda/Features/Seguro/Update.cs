﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Seguro
{
    public class Update
    {
        public class Input
        {
            public decimal IdSeguro { get; set; }
            public string NumeroPoliza { get; set; }
            public decimal IdTipoSeguro { get; set; }
            public decimal IdAseguradora { get; set; }
            public decimal IdUsuario { get; set; }
            public DateTime Vigencia { get; set; }
            public string Ahorro { get; set; }
        }

        public class Output
        {
            public decimal IdSeguro { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var SeguroExist = await _context.Seguros.AnyAsync(cancellationToken);
                if (!SeguroExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.NumeroPoliza);
                }

                Entities.Seguro seguro = _mapper.Map<Entities.Seguro>(request.Input);
                _context.Update(seguro);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdSeguro = seguro.IdSeguro });
            }
        }
    }
}
