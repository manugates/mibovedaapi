﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Seguro
{
    public class Delete
    {
        public class Input
        {
            public decimal IdSeguro { get; set; }

        }

        public class Outut
        {
            public decimal IdSeguro { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var seguro = await _context.Seguros
                    .Where(x => x.IdSeguro == request.Input.IdSeguro)
                    .ToListAsync(cancellationToken);

                if (seguro.Count() == 0)
                {
  
                }

                var seguroDelete = seguro.FirstOrDefault();

                _context.Seguros.Remove(seguroDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdSeguro = seguroDelete.IdSeguro;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
