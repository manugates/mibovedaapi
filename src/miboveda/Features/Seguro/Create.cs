﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Constants;
using miboveda.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace miboveda.Features.Seguro
{
    public class Create
    {
        public class Input
        {
            public decimal IdSeguro { get; set; }
            public string NumeroPoliza { get; set; }
            public decimal IdTipoSeguro { get; set; }
            public decimal IdAseguradora { get; set; }
            public decimal IdUsuario { get; set; }
            public DateTime Vigencia { get; set; }
            public string Ahorro { get; set; }
        }

        public class Output
        {
            public decimal IdSeguro { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {

        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public decimal GetMySequence()
            {
                var stringSeq = Database.Sequences.SeguroSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {

                decimal code = 3;
                request.Input.IdSeguro = GetMySequence();
                var seguroCreate = _mapper.Map<Entities.Seguro>(request.Input);

                await _context.Seguros.AddAsync(seguroCreate);

                _context.SaveChanges();

                code = seguroCreate.IdSeguro;

                return code == -1
                    ? RequestResult<Output>.Fail("La inversion no se pudo crear")
                    : RequestResult<Output>.Success(new Output { IdSeguro = seguroCreate.IdSeguro });
            }
        }
    }
}
