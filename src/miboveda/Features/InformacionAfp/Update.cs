﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.InformacionAfp
{
    public class Update
    {
        public class Input
        {
            public decimal IdInformacionAfp { get; set; }
            public decimal IdTipoFondo { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdAfp { get; set; }
            public decimal IdTipoCotizacion { get; set; }
        }

        public class Output
        {
            public decimal IdInformacionAfp { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var InformacionAfpExist = await _context.InformacionAfps.AnyAsync(cancellationToken);
                if (!InformacionAfpExist)
                {
                    
                }

                Entities.InformacionAfp informacionAfp = _mapper.Map<Entities.InformacionAfp>(request.Input);
                _context.Update(informacionAfp);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdInformacionAfp = informacionAfp.IdInformacionAfp });
            }
        }
    }
}
