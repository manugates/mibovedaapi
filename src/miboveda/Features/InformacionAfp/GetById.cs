﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.InformacionAfp
{
    public class GetById
    {
        public class Input
        {
            public decimal IdInformacionAfp { get; set; }
        }

        public class Output
        {
            public decimal IdInformacionAfp { get; set; }
            public string IdTipoFondo { get; set; }
            public decimal IdUsuario { get; set; }
            public string IdAfp { get; set; }
            public string IdTipoCotizacion { get; set; }
            public decimal ValorAfp { get; set; }
            public decimal AfpVal { get; set; }
            public decimal TipoFondoVal { get; set; }
            public decimal TipoCotizacionVal { get; set; }

        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from InformacionAfp in _context.InformacionAfps
                                    where InformacionAfp.IdUsuario == request.Input.IdInformacionAfp
                                    join afp in _context.Afps on InformacionAfp.IdAfp equals afp.IdAfp
                                    join tipoCotizacion in _context.TipoCotizaciones on InformacionAfp.IdTipoCotizacion equals tipoCotizacion.IdTipoCotizacion
                                    join tipoFondo in _context.TipoFondos on InformacionAfp.IdTipoFondo equals tipoFondo.IdTipoFondo

                                    select new Output
                                    {
                                        IdInformacionAfp = InformacionAfp.IdInformacionAfp,
                                        IdTipoFondo = tipoFondo.Descripcion,
                                        IdUsuario = InformacionAfp.IdUsuario,
                                        IdAfp = afp.Nombre,
                                        IdTipoCotizacion = tipoCotizacion.Descripcion,
                                        ValorAfp = InformacionAfp.IdAfp,
                                        AfpVal = InformacionAfp.IdAfp,
                                        TipoCotizacionVal = InformacionAfp.IdTipoCotizacion,
                                        TipoFondoVal = InformacionAfp.IdTipoFondo


        })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
