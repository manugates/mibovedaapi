﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Constants;
using miboveda.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace miboveda.Features.Empresa
{
    public class Create
    {
        public class Input
        {
            public decimal IdEmpresa { get; set; }
            public string Nombre { get; set; }
            public string RazonSocial { get; set; }
            public DateTime FechaIngreso { get; set; }
            public string Direccion { get; set; }
            public string Cargo { get; set; }
            public string Profesion { get; set; }
            public decimal IdCajaCompensacion { get; set; }
            public string NombreContacto { get; set; }
            public string ApellidoPaterno { get; set; }
            public string ApellidoMaterno { get; set; }
            public string Email { get; set; }
            public string Telefono { get; set; }
            public string Rut { get; set; }
            public decimal Comuna { get; set; }
            public decimal IdUsuario { get; set; }
        }

        public class Output
        {
            public decimal IdEmpresa { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {

        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public decimal GetMySequence()
            {
                var stringSeq = Database.Sequences.EmpresaSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }



            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {

                decimal code = 3;
                request.Input.IdEmpresa = GetMySequence();
                var empresaCreate = _mapper.Map<Entities.Empresa>(request.Input);

                await _context.Empresas.AddAsync(empresaCreate);

                _context.SaveChanges();

                code = empresaCreate.IdEmpresa;

                return code == -1
                    ? RequestResult<Output>.Fail("La empresa no se pudo crear")
                    : RequestResult<Output>.Success(new Output { IdEmpresa = empresaCreate.IdEmpresa });
            }
        }
    }
}
