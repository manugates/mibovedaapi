﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Empresa
{
    public class GetById
    {
        public class Input
        {
            public decimal IdUsuario { get; set; }
        }

        public class Output
        {
            public decimal IdEmpresa { get; set; }
            public string Nombre { get; set; }
            public string RazonSocial { get; set; }
            public string FechaIngreso { get; set; }
            public string Direccion { get; set; }
            public string Cargo { get; set; }
            public string Profesion { get; set; }
            public string IdCajaCompensacion { get; set; }
            public string NombreContacto { get; set; }
            public string ApellidoPaterno { get; set; }
            public string ApellidoMaterno { get; set; }
            public string Email { get; set; }
            public string Telefono { get; set; }
            public string Rut { get; set; }
            public decimal ValorCaja { get; set; }
            public string Comuna { get; set; }
            public string IdCaja { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdComuna { get; set; }
            public string FechaIngresoVal { get; set; }
         
        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from Empresa in _context.Empresas
                                    join caja in _context.CajaCompensaciones on Empresa.IdCajaCompensacion equals caja.IdCaja
                                    join comuna in _context.Comunas on Empresa.Comuna equals comuna.IdComuna
                                    where Empresa.IdUsuario == request.Input.IdUsuario
                                    select new Output
                                    {
                                        IdEmpresa = Empresa.IdEmpresa,
                                        Nombre = Empresa.Nombre,
                                        RazonSocial = Empresa.RazonSocial,
                                        FechaIngreso = Empresa.FechaIngreso.ToString(),
                                        Direccion = Empresa.Direccion,
                                        Cargo = Empresa.Cargo,
                                        Profesion = Empresa.Profesion,
                                        IdCajaCompensacion = caja.Nombre,
                                        NombreContacto = Empresa.NombreContacto,
                                        ApellidoPaterno = Empresa.ApellidoPaterno,
                                        ApellidoMaterno = Empresa.ApellidoMaterno,
                                        Email = Empresa.Email,
                                        Telefono = Empresa.Telefono,
                                        Rut = Empresa.Rut,
                                        ValorCaja = Empresa.IdCajaCompensacion,
                                        Comuna = comuna.Nombre,
                                        IdCaja = Empresa.IdCajaCompensacion.ToString(),
                                        IdComuna = Empresa.Comuna,
                                        FechaIngresoVal = Empresa.FechaIngreso.ToString("dd-MM-yyyy")


                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
