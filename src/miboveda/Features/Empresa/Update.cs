﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Empresa
{
    public class Update
    {
        public class Input
        {
            public decimal IdEmpresa { get; set; }
            public string Nombre { get; set; }
            public string RazonSocial { get; set; }
            public DateTime FechaIngreso { get; set; }
            public string Direccion { get; set; }
            public string Cargo { get; set; }
            public string Profesion { get; set; }
            public decimal IdCajaCompensacion { get; set; }
            public string NombreContacto { get; set; }
            public string ApellidoPaterno { get; set; }
            public string ApellidoMaterno { get; set; }
            public string Email { get; set; }
            public string Telefono { get; set; }
            public string Rut { get; set; }
            public decimal Comuna { get; set; }
            public decimal IdUsuario { get; set; }
        }

        public class Output
        {
            public decimal IdEmpresa { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var EmpresaExist = await _context.Empresas.AnyAsync(cancellationToken);
                if (!EmpresaExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Nombre);
                }

                Entities.Empresa empresa = _mapper.Map<Entities.Empresa>(request.Input);
                _context.Update(empresa);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdEmpresa = empresa.IdEmpresa });
            }
        }
    }
}
