﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.InformacionCaja
{
    public class Update
    {
        public class Input
        {
            public decimal IdInformacionCaja { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdCajaCompensacion { get; set; }
        }

        public class Output
        {
            public decimal IdInformacionCaja { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var InformacionCajaExist = await _context.InformacionCajas.AnyAsync(cancellationToken);
                if (!InformacionCajaExist)
                {

                }

                Entities.InformacionCaja informacionCaja = _mapper.Map<Entities.InformacionCaja>(request.Input);
                _context.Update(informacionCaja);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdInformacionCaja = informacionCaja.IdInformacionCaja });
            }
        }
    }
}
