﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;


namespace miboveda.Features.InformacionCaja
{
    public class GetById
    {
        public class Input
        {
            public decimal IdInformacionCaja { get; set; }
        }

        public class Output
        {
            public decimal IdInformacionCaja { get; set; }
            public decimal IdUsuario { get; set; }
            public string IdCajaCompensacion { get; set; }
            public decimal CajaCompensacionVal { get; set; }

        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from InformacionCaja in _context.InformacionCajas
                                    where InformacionCaja.IdUsuario == request.Input.IdInformacionCaja
                                    join caja in _context.CajaCompensaciones on InformacionCaja.IdCajaCompensacion equals caja.IdCaja

                                    select new Output
                                    {
                                        IdInformacionCaja = InformacionCaja.IdInformacionCaja,
                                        IdUsuario = InformacionCaja.IdUsuario,
                                        IdCajaCompensacion = caja.Nombre,
                                        CajaCompensacionVal = InformacionCaja.IdCajaCompensacion


                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
