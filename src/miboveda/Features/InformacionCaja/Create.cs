﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Constants;
using miboveda.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace miboveda.Features.InformacionCaja
{
    public class Create
    {
        public class Input
        {
            public decimal IdInformacionCaja { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdCajaCompensacion { get; set; }
        }

        public class Output
        {
            public decimal IdInformacionCaja { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {

        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public decimal GetMySequence()
            {
                var stringSeq = Database.Sequences.InformacionCajaSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }



            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {

                decimal code = 3;
                request.Input.IdInformacionCaja = GetMySequence();
                var informacionCajaCreate = _mapper.Map<Entities.InformacionCaja>(request.Input);

                await _context.InformacionCajas.AddAsync(informacionCajaCreate);

                _context.SaveChanges();

                code = informacionCajaCreate.IdInformacionCaja;

                return code == -1
                    ? RequestResult<Output>.Fail("La empresa no se pudo crear")
                    : RequestResult<Output>.Success(new Output { IdInformacionCaja = informacionCajaCreate.IdInformacionCaja });
            }
        }

    }
}
