﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.CajaCompensacion
{
    public class Update
    {
        public class Input
        {
            public decimal IdCaja { get; set; }
            public string Nombre { get; set; }
        }

        public class Output
        {
            public decimal IdCaja { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var CajaCompensacionExist = await _context.CajaCompensaciones.AnyAsync(cancellationToken);
                if (!CajaCompensacionExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Nombre);
                }

                Entities.CajaCompensacion cajacompensacion = _mapper.Map<Entities.CajaCompensacion>(request.Input);
                _context.Update(cajacompensacion);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdCaja = cajacompensacion.IdCaja });
            }
        }
    }
}
