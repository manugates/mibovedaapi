﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.CajaCompensacion
{
    public class Delete
    {
        public class Input
        {
            public string Nombre { get; set; }

        }

        public class Outut
        {
            public decimal IdCaja { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var cajacompensacion = await _context.CajaCompensaciones
                    .Where(x => x.Nombre == request.Input.Nombre)
                    .ToListAsync(cancellationToken);

                if (cajacompensacion.Count() == 0)
                {
                    return RequestResult<Outut>.RecordNotFound(request.Input.Nombre);
                }

                var cajacompensacionDelete = cajacompensacion.FirstOrDefault();

                _context.CajaCompensaciones.Remove(cajacompensacionDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdCaja = cajacompensacionDelete.IdCaja;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
