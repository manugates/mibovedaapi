﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.TipoBien
{
    public class Create
    {
        public class Input
        {
            public decimal IdTipoBien { get; set; }
            public string Descripcion { get; set; }
        }

        public class Output
        {
            public decimal IdTipoBien { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {

        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }



            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {

                var code = 3;
                var tipoBienCreate = _mapper.Map<Entities.TipoBien>(request.Input);

                await _context.TipoBienes.AddAsync(tipoBienCreate);

                code = _context.SaveChanges();

                var output = new Output();


                return code == 3
                    ? RequestResult<Output>.RecordNotFound()
                    : RequestResult<Output>.Success(output);
            }
        }
    }
}
