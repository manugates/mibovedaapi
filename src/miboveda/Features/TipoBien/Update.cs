﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.TipoBien
{
    public class Update
    {
        public class Input
        {
            public decimal IdTipoBien { get; set; }
            public string Descripcion { get; set; }
        }

        public class Output
        {
            public decimal IdTipoBien { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var TipoBienExist = await _context.TipoBienes.AnyAsync(cancellationToken);
                if (!TipoBienExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Descripcion);
                }

                Entities.TipoBien tipobien = _mapper.Map<Entities.TipoBien>(request.Input);
                _context.Update(tipobien);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdTipoBien = tipobien.IdTipoBien });
            }
        }
    }
}
