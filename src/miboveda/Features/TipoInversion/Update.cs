﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.TipoInversion
{
    public class Update
    {
        public class Input
        {
            public decimal IdTipoInversion { get; set; }
            public string Descripcion { get; set; }
        }

        public class Output
        {
            public decimal IdTipoInversion { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var TipoInversionExist = await _context.TipoInversiones.AnyAsync(cancellationToken);
                if (!TipoInversionExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Descripcion);
                }

                Entities.TipoInversion tipoinversion = _mapper.Map<Entities.TipoInversion>(request.Input);
                _context.Update(tipoinversion);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdTipoInversion = tipoinversion.IdTipoInversion });
            }
        }
    }
}
