﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.TipoInversion
{
    public class Delete
    {
        public class Input
        {
            public string Descripcion { get; set; }

        }

        public class Outut
        {
            public decimal IdTipoInversion { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var tipoinversion = await _context.TipoInversiones
                    .Where(x => x.Descripcion == request.Input.Descripcion)
                    .ToListAsync(cancellationToken);

                if (tipoinversion.Count() == 0)
                {
                    return RequestResult<Outut>.RecordNotFound(request.Input.Descripcion);
                }

                var tipoinversionDelete = tipoinversion.FirstOrDefault();

                _context.TipoInversiones.Remove(tipoinversionDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdTipoInversion = tipoinversionDelete.IdTipoInversion;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
