﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using miboveda.Features.Shared;

namespace miboveda.Features.Usuario
{
    [Route("api/Usuario")]
    public class UsuarioController : BaseController
    {
        public UsuarioController(IMediator mediator) : base(mediator)
        {

        }

        [HttpPost]
        [ProducesResponseType(typeof(Create.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Post([FromBody]Create.Input input)
        {
            var request = new Create.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpGet("{IdUsuario}")]
        [ProducesResponseType(typeof(GetById.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(GetById.Input input)
        {
            var request = new GetById.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpPatch]
        [ProducesResponseType(typeof(Update.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Patch([FromBody]Update.Input input)
        {
            var request = new Update.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpPost]
        [Route("delete")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete([FromBody]Delete.Input input)
        {
            var request = new Delete.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpGet]
        [Route("getRut")]
        [ProducesResponseType(typeof(GetRuts.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRuts()
        {
            var request = new GetRuts.Request();
            return await HandleRequestAsync(request);
        }

        [HttpPost]
        [Route("login")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Login([FromBody]Login.Input input)
        {
            var request = new Login.Request(input);
            return await HandleRequestAsync(request);
        }
    }
}
