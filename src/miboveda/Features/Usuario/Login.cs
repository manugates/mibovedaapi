﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Usuario
{
    public class Login
    {
        public class Input
        {
            public string Rut { get; set; }
            public string Password { get; set; }
        }

        public class Output
        {
            public int Code { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {
                
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;


            public Handler(MiBovedaDbContext context)
            {
                _context = context;

            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                
                var usuario = await _context.Usuarios
                    .Where(x => x.Rut == request.Input.Rut && x.Password == request.Input.Password)
                    .ToListAsync(cancellationToken);


                return usuario.Any()
                    ? RequestResult<Output>.Success(new Output { Code = 0 })
                    : RequestResult<Output>.RecordNotFound(request.Input.Rut);

            }
        }
    }
}
