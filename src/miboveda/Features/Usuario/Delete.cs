﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Usuario
{
    public class Delete
    {
        public class Input
        {
            public string Rut { get; set; }

        }

        public class Outut
        {
            public decimal IdUsuario { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var usuario = await _context.Usuarios
                    .Where(x => x.Rut == request.Input.Rut)
                    .ToListAsync(cancellationToken);

                if (usuario.Count() == 0)
                {
                    return RequestResult<Outut>.RecordNotFound(request.Input.Rut);
                }

                var usuarioDelete = usuario.FirstOrDefault();

                _context.Usuarios.Remove(usuarioDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdUsuario = usuarioDelete.IdUsuario;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
