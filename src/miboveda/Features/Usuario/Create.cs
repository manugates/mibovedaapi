﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Constants;
using miboveda.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System.Net.Mail;
using System.Text;
using System.Net.Mime;

namespace miboveda.Features.Usuario
{
    public class Create
    {
        public class Input
        {
            public decimal IdUsuario { get; set; }
            public string Rut { get; set; }
            public string Password { get; set; }
            public string Email { get; set; }
            public DateTime FechaNacimiento { get; set; }
            public DateTime FechaInscripcion { get; set; }
            public string Nombre { get; set; }
            public string ApellidoPaterno { get; set; }
            public string ApellidoMaterno { get; set; }
            public DateTime FechaCreacion { get; set; }
            public DateTime FechaUpdate { get; set; }
            public decimal CreadoPor { get; set; }
            public decimal IdAfp { get; set; }
            public decimal IdIsapre { get; set; }
            public decimal IdPlan { get; set; }
            public string Telefono { get; set; }
            public string Comuna { get; set; }
        }

        public class Output
        {
            public decimal IdUsuario { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {

        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public decimal GetMySequence()
            {
                var stringSeq = Database.Sequences.UsuarioSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }

            public decimal GetMySequenceAfp()
            {
                var stringSeq = Database.Sequences.InformacionAfpSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }

            public decimal GetMySequenceIsapre()
            {
                var stringSeq = Database.Sequences.InformacionIsapreSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }

            public decimal GetMySequenceCaja()
            {
                var stringSeq = Database.Sequences.InformacionCajaSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }

            public decimal GetMySequenceEmpresa()
            {
                var stringSeq = Database.Sequences.EmpresaSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }

            public bool SendEmail(string nombreUsuario, string password, string correo)
            {
                var send = true;
                /* Empiezo a leer la plantilla de correo Titular */
                string Body = System.IO.File.ReadAllText("Plantilla/emailUsuario.html");
                /* Realizo los reemplazos necesarios */
                Body = Body.Replace("#NombreUsuario", nombreUsuario).Replace("#Contraseña", password);
                //Agrego las imagenes al html
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(Body, Encoding.UTF8, MediaTypeNames.Text.Html);
                LinkedResource imgLogo = new LinkedResource("Plantilla/logo.png", MediaTypeNames.Image.Jpeg);
                imgLogo.ContentId = "logo";
                LinkedResource imgBoveda = new LinkedResource("Plantilla/boveda.jpg", MediaTypeNames.Image.Jpeg);
                imgBoveda.ContentId = "boveda";

                htmlView.LinkedResources.Add(imgLogo);
                htmlView.LinkedResources.Add(imgBoveda);
                //Empiezo a crear los datos del envio del correo
                MailMessage message = new MailMessage();
                message.From = new MailAddress("no-responder@miboveda.cl");
                message.To.Add(correo);
                message.Subject = "Bienvenido a Mi Bóveda";
                message.IsBodyHtml = true;

                message.AlternateViews.Add(htmlView);
                //configuracion del envio del correo
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Host = "mail.miboveda.cl";
                smtpClient.Port = 25;
                smtpClient.EnableSsl = false;

                smtpClient.Credentials = new System.Net.NetworkCredential("no-responder@miboveda.cl", "pga123654");

                try
                {
                    //Se envia correo
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteServerCertificateValidationCallback);
                    smtpClient.Send(message);
                } catch(Exception ex)
                {
                    return !send;
                }

                return send;
            }

            private bool RemoteServerCertificateValidationCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                //Console.WriteLine(certificate);
                return true;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                decimal code = -1;
                request.Input.IdUsuario = GetMySequence();
                request.Input.FechaCreacion = DateTime.Now;
                request.Input.FechaInscripcion = DateTime.Now;
                request.Input.FechaUpdate = DateTime.Now;

                var usuarioCreate = _mapper.Map<Entities.Usuario>(request.Input);


                var info_afpCreate = new Entities.InformacionAfp();

                info_afpCreate.IdInformacionAfp = GetMySequenceAfp();
                info_afpCreate.IdAfp = 0;
                info_afpCreate.IdTipoCotizacion = 0;
                info_afpCreate.IdUsuario = request.Input.IdUsuario;
                info_afpCreate.IdTipoFondo = 0;


                var info_isapreCreate = new Entities.InformacionIsapre();

                info_isapreCreate.IdInformacionIsapre = GetMySequenceIsapre();
                info_isapreCreate.IdIsapre = 0;
                info_isapreCreate.IdUsuario = request.Input.IdUsuario;
                info_isapreCreate.IdTipoIsapre = 0;


                var info_cajaCreate = new Entities.InformacionCaja();

                info_cajaCreate.IdInformacionCaja = GetMySequenceCaja();
                info_cajaCreate.IdCajaCompensacion = 0;
                info_cajaCreate.IdUsuario = request.Input.IdUsuario;


                var empresaCreate = new Entities.Empresa();

                empresaCreate.IdEmpresa = GetMySequenceEmpresa();
                empresaCreate.Nombre = "Sin Informacion";
                empresaCreate.RazonSocial = "Sin Informacion";
                empresaCreate.FechaIngreso = DateTime.Now;
                empresaCreate.Direccion = "Sin Informacion";
                empresaCreate.Cargo = "Sin Informacion";
                empresaCreate.Profesion = "Sin Informacion";
                empresaCreate.IdCajaCompensacion = 0;
                empresaCreate.Nombre = "Sin Informacion";
                empresaCreate.ApellidoPaterno = "Sin Informacion";
                empresaCreate.ApellidoMaterno = "Sin Informacion";
                empresaCreate.Email = "Sin Informacion";
                empresaCreate.Telefono = "Sin Informacion";
                empresaCreate.Rut = "Sin Informacion";
                empresaCreate.Comuna = 2;
                empresaCreate.IdUsuario = request.Input.IdUsuario;


                await _context.Usuarios.AddAsync(usuarioCreate);
                await _context.InformacionAfps.AddAsync(info_afpCreate);
                await _context.InformacionIsapres.AddAsync(info_isapreCreate);
                await _context.InformacionCajas.AddAsync(info_cajaCreate);
                await _context.Empresas.AddAsync(empresaCreate);

                _context.SaveChanges();

                code = usuarioCreate.IdUsuario;
                var envio = SendEmail(usuarioCreate.Nombre, usuarioCreate.Password, usuarioCreate.Email);


                return code == -1
                    ? RequestResult<Output>.Fail("El usuario no se pudo crear")
                    : RequestResult<Output>.Success(new Output { IdUsuario = 1 });
            }
        }
    }
}
