﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Usuario
{
    public class GetRuts
    {
        public class Output
        {
            public string Rut { get; set; }
        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {

            public Request()
            {
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from usuario in _context.Usuarios

                                    select new Output
                                    {
                                        Rut = usuario.Rut

                                    }).ToListAsync(cancellationToken);

                return result.Any()
                    ? RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result))
                    : RequestResult<List<Output>>.RecordNotFound();

            }
        }
    }
}
