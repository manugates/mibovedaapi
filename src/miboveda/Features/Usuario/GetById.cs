﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Usuario
{
    public class GetById
    {
        public class Input
        {
            public decimal IdUsuario { get; set; }
        }

        public class Output
        {
            public decimal IdUsuario { get; set; }
            public string Rut { get; set; }
            public string Password { get; set; }
            public string Email { get; set; }
            public string FechaNacimiento { get; set; }
            public DateTime FechaInscripcion { get; set; }
            public string Nombre { get; set; }
            public string ApellidoPaterno { get; set; }
            public string ApellidoMaterno { get; set; }
            public DateTime FechaCreacion { get; set; }
            public DateTime FechaUpdate { get; set; }
            public decimal CreadoPor { get; set; }
            public decimal IdAfp { get; set; }
            public decimal IdIsapre { get; set; }
            public decimal IdPlan { get; set; }
            public string Telefono { get; set; }
            public string Comuna { get; set; }
            public string FechaNacimientoVal { get; set; }


        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from Usuario in _context.Usuarios
                                    where Usuario.Rut == request.Input.IdUsuario.ToString()
                                    select new Output
                                    {
                                        IdUsuario  = Usuario.IdUsuario,
                                        Rut = Usuario.Rut,
                                        Password = Usuario.Password,
                                        Email = Usuario.Email, 
                                        FechaNacimiento = Usuario.FechaNacimiento.ToString(),
                                        FechaInscripcion = Usuario.FechaInscripcion, 
                                        Nombre = Usuario.Nombre,
                                        ApellidoPaterno = Usuario.ApellidoPaterno,
                                        ApellidoMaterno = Usuario.ApellidoMaterno,
                                        FechaCreacion = Usuario.FechaCreacion,
                                        FechaUpdate = Usuario.FechaUpdate,
                                        CreadoPor = Usuario.CreadoPor,
                                        IdAfp = Usuario.IdAfp,
                                        IdIsapre = Usuario.IdIsapre,
                                        IdPlan = Usuario.IdPlan,
                                        Telefono = Usuario.Telefono,
                                        Comuna = Usuario.Comuna,
                                        FechaNacimientoVal = Usuario.FechaNacimiento.ToString("dd-MM-yyyy")


                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
