﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;
using System.Linq;


namespace miboveda.Features.Usuario
{
    public class Update
    {
        public class Input
        {
            public decimal IdUsuario { get; set; }
            public string Rut { get; set; }
            public string Password { get; set; }
            public string Email { get; set; }
            public DateTime FechaNacimiento { get; set; }
            public DateTime FechaInscripcion { get; set; }
            public string Nombre { get; set; }
            public string ApellidoPaterno { get; set; }
            public string ApellidoMaterno { get; set; }
            public DateTime FechaCreacion { get; set; }
            public DateTime FechaUpdate { get; set; }
            public decimal CreadoPor { get; set; }
            public decimal IdAfp { get; set; }
            public decimal IdIsapre { get; set; }
            public decimal IdPlan { get; set; }
            public string Telefono { get; set; }
            public string Comuna { get; set; }
        }

        public class Output
        {
            public decimal IdUsuario { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var usuarioExist = await _context.Usuarios.AnyAsync(cancellationToken);
                if (!usuarioExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Nombre);
                }

                request.Input.FechaUpdate = DateTime.Now;
       

                Entities.Usuario usuario = _mapper.Map<Entities.Usuario>(request.Input);
                _context.Update(usuario);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdUsuario = usuario.IdUsuario });
            }
        }
    }
}
