﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.TipoDeuda
{
    public class Update
    {
        public class Input
        {
            public decimal IdTipoDeuda { get; set; }
            public string Descripcion { get; set; }
        }

        public class Output
        {
            public decimal IdTipoDeuda { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var TipoDeudaExist = await _context.TipoDeudas.AnyAsync(cancellationToken);
                if (!TipoDeudaExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Descripcion);
                }

                Entities.TipoDeuda tipodeuda = _mapper.Map<Entities.TipoDeuda>(request.Input);
                _context.Update(tipodeuda);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdTipoDeuda = tipodeuda.IdTipoDeuda });
            }
        }
    }
}
