﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.TipoSeguro
{
    public class GetById
    {
        public class Input
        {
            public decimal IdTipoSeguro { get; set; }
        }

        public class Output
        {
            public decimal IdTipoSeguro { get; set; }
            public string Descripcion { get; set; }
        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from TipoSeguro in _context.TipoSeguros
                                    where TipoSeguro.IdTipoSeguro == request.Input.IdTipoSeguro
                                    select new Output
                                    {
                                        IdTipoSeguro = TipoSeguro.IdTipoSeguro,
                                        Descripcion = TipoSeguro.Descripcion


                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
