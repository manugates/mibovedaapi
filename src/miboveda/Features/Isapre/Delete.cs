﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Isapre
{
    public class Delete
    {
        public class Input
        {
            public string Nombre { get; set; }

        }

        public class Outut
        {
            public decimal IdIsapre { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var isapre = await _context.Isapres
                    .Where(x => x.Nombre == request.Input.Nombre)
                    .ToListAsync(cancellationToken);

                if (isapre.Count() == 0)
                {
                    return RequestResult<Outut>.RecordNotFound(request.Input.Nombre);
                }

                var isapreDelete = isapre.FirstOrDefault();

                _context.Isapres.Remove(isapreDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdIsapre = isapreDelete.IdIsapre;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
