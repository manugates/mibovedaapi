﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Cuenta
{
    public class CuentaExiste
    {
        public class Input
        {
            public string Numero { get; set; }
            public decimal IdBanco { get; set; }
        }

        public class Output
        {
            public decimal IdCuenta { get; set; }
            public string Numero { get; set; }
            public string IdBanco { get; set; }
            public string IdTipoCuenta { get; set; }
            public decimal IdUsuario { get; set; }
        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from Cuenta in _context.Cuentas
                                    join banco in _context.Bancos on Cuenta.IdBanco equals banco.IdBanco
                                    join tipoCuenta in _context.TipoCuentas on Cuenta.IdTipoCuenta equals tipoCuenta.IdTipoCuenta
                                    where Cuenta.IdBanco == request.Input.IdBanco && Cuenta.Numero == request.Input.Numero
                                    select new Output
                                    {
                                        IdCuenta = Cuenta.IdCuenta,
                                        Numero = Cuenta.Numero,
                                        IdBanco = banco.Nombre,
                                        IdTipoCuenta = tipoCuenta.Descripcion,
                                        IdUsuario = Cuenta.IdUsuario

                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
