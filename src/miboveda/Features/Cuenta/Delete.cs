﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Cuenta
{
    public class Delete
    {
        public class Input
        {
            public decimal IdCuenta { get; set; }

        }

        public class Outut
        {
            public decimal IdCuenta { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var cuenta = await _context.Cuentas
                    .Where(x => x.IdCuenta == request.Input.IdCuenta)
                    .ToListAsync(cancellationToken);

                if (cuenta.Count() == 0)
                {
                  
                }

                var cuentaDelete = cuenta.FirstOrDefault();

                _context.Cuentas.Remove(cuentaDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdCuenta = cuentaDelete.IdCuenta;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
