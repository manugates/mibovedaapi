﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Constants;
using miboveda.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace miboveda.Features.Cuenta
{
    public class Create
    {
        public class Input
        {
            public decimal IdCuenta { get; set; }
            public string Numero { get; set; }
            public decimal IdBanco { get; set; }
            public decimal IdTipoCuenta { get; set; }
            public decimal IdUsuario { get; set; }

        }

        public class Output
        {
            public decimal IdCuenta { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {

        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public decimal GetMySequence()
            {
                var stringSeq = Database.Sequences.CuentaSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }



            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {

                decimal code = 3;
                request.Input.IdCuenta = GetMySequence();
                var cuentaCreate = _mapper.Map<Entities.Cuenta>(request.Input);

                await _context.Cuentas.AddAsync(cuentaCreate);

                _context.SaveChanges();

                code = cuentaCreate.IdCuenta;

                return code == -1
                    ? RequestResult<Output>.Fail("La cuenta no se pudo crear")
                    : RequestResult<Output>.Success(new Output { IdCuenta = cuentaCreate.IdCuenta });
            }
        }
    }
}
