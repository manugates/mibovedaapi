﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Cuenta
{
    public class Update
    {
        public class Input
        {
            public decimal IdCuenta { get; set; }
            public decimal Numero { get; set; }
            public decimal IdBanco { get; set; }
            public decimal IdTipoCuenta { get; set; }
            public decimal IdUsuario { get; set; }
        }

        public class Output
        {
            public decimal IdCuenta { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var CuentaExist = await _context.Cuentas.AnyAsync(cancellationToken);
                if (!CuentaExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Numero.ToString());
                }

                Entities.Cuenta cuenta = _mapper.Map<Entities.Cuenta>(request.Input);
                _context.Update(cuenta);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdCuenta = cuenta.IdCuenta });
            }
        }
    }
}
