﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.TipoArchivo
{
    public class Delete
    {
        public class Input
        {
            public string Descripcion { get; set; }

        }

        public class Outut
        {
            public decimal IdTipoArchivo { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var tipoarchivo = await _context.TipoArchivos
                    .Where(x => x.Descripcion == request.Input.Descripcion)
                    .ToListAsync(cancellationToken);

                if (tipoarchivo.Count() == 0)
                {
                    return RequestResult<Outut>.RecordNotFound(request.Input.Descripcion);
                }

                var tipoarchivoDelete = tipoarchivo.FirstOrDefault();

                _context.TipoArchivos.Remove(tipoarchivoDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdTipoArchivo = tipoarchivoDelete.IdTipoArchivo;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
