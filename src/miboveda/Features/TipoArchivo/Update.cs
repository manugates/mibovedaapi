﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.TipoArchivo
{
    public class Update
    {
        public class Input
        {
            public decimal IdTipoArchivo { get; set; }
            public string Descripcion { get; set; }
        }

        public class Output
        {
            public decimal IdTipoArchivo { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var TipoArchivoExist = await _context.TipoArchivos.AnyAsync(cancellationToken);
                if (!TipoArchivoExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Descripcion);
                }

                Entities.TipoArchivo tipoarchivo = _mapper.Map<Entities.TipoArchivo>(request.Input);
                _context.Update(tipoarchivo);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdTipoArchivo = tipoarchivo.IdTipoArchivo });
            }
        }
    }
}
