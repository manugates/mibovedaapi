﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace miboveda.Features.Shared
{
    public class RequestResult <TResult>
    {
        public TResult Value { get; }
        public List<string> Errors { get; }
        public bool IsSuccess => !Errors.Any();

        public static RequestResult<TResult> RecordNotFound()
        {
            return new RequestResult<TResult>(new[] { $"No existe registro" });
        }

        public static RequestResult<TResult> RecordNotFound(int id)
        {
            return new RequestResult<TResult>(new[] { $"No existe registro con id {id}" });
        }

        public static RequestResult<TResult> RecordNotFound(string code)
        {
            return new RequestResult<TResult>(new[] { $"No existe registro con código {code}" });
        }

        public static RequestResult<TResult> RecordNotFound(string field, int id)
        {
            return new RequestResult<TResult>(new[] { $"No existe {field} con id {id}" });
        }

        public static RequestResult<TResult> RecordNotFound(string field, string id)
        {
            return new RequestResult<TResult>(new[] { $"No existe {field} con id {id}" });
        }

        public static RequestResult<TResult> AlreadyExists(string field, string value)
        {
            return new RequestResult<TResult>(new[] { $"Ya existe un registro con {field}: {value}" });
        }

        public static RequestResult<TResult> Fail(string error)
        {
            return new RequestResult<TResult>(new[] { error });
        }

        public static RequestResult<TResult> Fail(IEnumerable<string> errors)
        {
            return new RequestResult<TResult>(errors);
        }

        public static RequestResult<TResult> Success(TResult value)
        {
            return new RequestResult<TResult>(value);
        }

        public static implicit operator ActionResult(RequestResult<TResult> requestResult)
        {
            if (requestResult == null)
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }

            if (requestResult.IsSuccess)
            {
                return new OkObjectResult(requestResult.Value);
            }
            else
            {
                return new BadRequestObjectResult(requestResult.Errors);
            }
        }

        private RequestResult(TResult value)
        {
            Value = value;
            Errors = new List<string>();
        }

        private RequestResult(IEnumerable<string> errors)
        {
            Errors = new List<string>(errors);
        }
    }
}
