﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using miboveda.Features.Shared.Errors;
using miboveda.Infrastructure.Constants;

namespace miboveda.Features.Shared
{
    public class BaseController : Controller
    {
        public IMediator Mediator { get; }

        public BaseController(IMediator mediator)
        {
            Mediator = mediator;
        }

        protected async Task<IActionResult> HandleRequestAsync(dynamic request)
        {
            if (request == null)
            {
                var error = new ErrorResponse
                {
                    Error = new Error
                    {
                        Message = ResponseMessages.BadRequest.ReceivedMessage,
                        Details = new[] {
                            new ErrorDetail
                            {
                                Message = ResponseMessages.BadRequest.BodyWithNoUsableContent
                            }
                        }
                    }
                };

                return BadRequest(error);
            }

            ActionResult result = await Mediator.Send(request);

            return result;
        }

    }
}
