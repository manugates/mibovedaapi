﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Deuda
{
    public class Update
    {
        public class Input
        {
            public decimal IdDeuda { get; set; }
            public string Operacion { get; set; }
            public decimal Monto { get; set; }
            public string SeguroDegravament { get; set; }
            public string Mora { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdTipoDeuda { get; set; }
            public decimal IdBanco { get; set; }
            public decimal IdProducto { get; set; }
            public decimal IdCaja { get; set; }
            public decimal IdCasaComercial { get; set; }
            public decimal IdEntidad { get; set; }
        }

        public class Output
        {
            public decimal IdDeuda { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var DeudaExist = await _context.Deudas.AnyAsync(cancellationToken);
                if (!DeudaExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Operacion);
                }

                Entities.Deuda deuda = _mapper.Map<Entities.Deuda>(request.Input);
                _context.Update(deuda);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdDeuda = deuda.IdDeuda });
            }
        }
    }
}
