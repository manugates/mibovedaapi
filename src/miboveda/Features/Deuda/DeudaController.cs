﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using miboveda.Features.Shared;


namespace miboveda.Features.Deuda
{
    [Route("api/Deuda")]
    public class DeudaController : BaseController
    {
        public DeudaController(IMediator mediator) : base(mediator)
        {

        }

        [HttpPost]
        [ProducesResponseType(typeof(Create.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Post([FromBody]Create.Input input)
        {
            var request = new Create.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpGet("{IdDeuda}")]
        [ProducesResponseType(typeof(GetById.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(GetById.Input input)
        {
            var request = new GetById.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpGet]
        [Route("getList")]
        [ProducesResponseType(typeof(GetList.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            var request = new GetList.Request();
            return await HandleRequestAsync(request);
        }

        [HttpGet]
        [Route("getOne/{IdDeuda}")]
        [ProducesResponseType(typeof(GetOne.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(GetOne.Input input)
        {
            var request = new GetOne.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpPatch]
        [ProducesResponseType(typeof(Update.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Patch([FromBody]Update.Input input)
        {
            var request = new Update.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpPost]
        [Route("delete")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete([FromBody]Delete.Input input)
        {
            var request = new Delete.Request(input);
            return await HandleRequestAsync(request);
        }
    }
}
