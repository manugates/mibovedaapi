﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Deuda
{
    public class Delete
    {
        public class Input
        {
            public decimal IdDeuda { get; set; }

        }

        public class Outut
        {
            public decimal IdDeuda { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var deuda = await _context.Deudas
                    .Where(x => x.IdDeuda == request.Input.IdDeuda)
                    .ToListAsync(cancellationToken);

                if (deuda.Count() == 0)
                {
                    
                }

                var deudaDelete = deuda.FirstOrDefault();

                _context.Deudas.Remove(deudaDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdDeuda = deudaDelete.IdDeuda;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
