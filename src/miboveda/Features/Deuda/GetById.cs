﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Deuda
{
    public class GetById
    {
        public class Input
        {
            public decimal IdDeuda { get; set; }
        }

        public class Output
        {
            public decimal IdDeuda { get; set; }
            public string Operacion { get; set; }
            public decimal Monto { get; set; }
            public string SeguroDegravament { get; set; }
            public string Mora { get; set; }
            public decimal IdUsuario { get; set; }
            public string IdTipoDeuda { get; set; }
            public string IdBanco { get; set; }
            public string IdProducto { get; set; }
            public string IdCaja { get; set; }
            public string IdCasaComercial { get; set; }
            public string IdEntidad { get; set; }
        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from Deuda in _context.Deudas
                                    where Deuda.IdUsuario == request.Input.IdDeuda
                                    join TipoDeuda in _context.TipoDeudas on Deuda.IdTipoDeuda equals TipoDeuda.IdTipoDeuda
                                    join Banco in _context.Bancos on Deuda.IdBanco equals Banco.IdBanco
                                    join Producto in _context.Productos on Deuda.IdProducto equals Producto.IdProducto
                                    join CajaCompensacion in _context.CajaCompensaciones on Deuda.IdCaja equals CajaCompensacion.IdCaja
                                    join CasaComercial in _context.CasasComerciales on Deuda.IdCasaComercial equals CasaComercial.IdCasaComercial
                                    join Entidad in _context.Entidades on Deuda.IdEntidad equals Entidad.IdEntidad
                                    select new Output
                                    {
                                        IdDeuda = Deuda.IdDeuda,
                                        Operacion = Deuda.Operacion,
                                        Monto = Deuda.Monto,
                                        SeguroDegravament = Deuda.SeguroDegravament,
                                        Mora = Deuda.Mora,
                                        IdUsuario = Deuda.IdUsuario,
                                        IdTipoDeuda = TipoDeuda.Descripcion,
                                        IdBanco = Banco.Nombre,
                                        IdProducto = Producto.Descripcion,
                                        IdCaja = CajaCompensacion.Nombre,
                                        IdCasaComercial = CasaComercial.Descripcion,
                                        IdEntidad = Entidad.Descripcion


                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
