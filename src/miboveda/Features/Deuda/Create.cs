﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Constants;
using miboveda.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace miboveda.Features.Deuda
{
    public class Create
    {
        public class Input
        {
            public decimal IdDeuda { get; set; }
            public string Operacion { get; set; }
            public decimal Monto { get; set; }
            public string SeguroDegravament { get; set; }
            public string Mora { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdTipoDeuda { get; set; }
            public decimal IdBanco { get; set; }
            public decimal IdProducto { get; set; }
            public decimal IdCaja { get; set; }
            public decimal IdCasaComercial { get; set; }
            public decimal IdEntidad { get; set; }
        }

        public class Output
        {
            public decimal IdDeuda { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {

        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public decimal GetMySequence()
            {
                var stringSeq = Database.Sequences.DeudaSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }



            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {

                decimal code = 3;
                request.Input.IdDeuda = GetMySequence();
                var deudaCreate = _mapper.Map<Entities.Deuda>(request.Input);

                await _context.Deudas.AddAsync(deudaCreate);

                _context.SaveChanges();

                code = deudaCreate.IdDeuda;

                return code == -1
                    ? RequestResult<Output>.Fail("La deuda no se pudo crear")
                    : RequestResult<Output>.Success(new Output { IdDeuda = deudaCreate.IdDeuda });
            }
        }
    }
}
