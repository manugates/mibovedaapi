﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Bienes
{
    public class Update
    {
        public class Input
        {
            public decimal IdBien { get; set; }
            public string Descripcion { get; set; }
            public decimal IdUsuario { get; set; }
            public string IdTipoBien { get; set; }
            public decimal IdTipoDominio { get; set; }
            public decimal IdTipoInmueble { get; set; }
            public decimal IdTipoMueble { get; set; }
        }

        public class Output
        {
            public decimal IdBien { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var BienExist = await _context.Bienes.AnyAsync(cancellationToken);
                if (!BienExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Descripcion);
                }

                Entities.Bienes bien = _mapper.Map<Entities.Bienes>(request.Input);
                _context.Update(bien);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdBien = bien.IdBien });
            }
        }
    }
}
