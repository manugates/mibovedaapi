﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Bienes
{
    public class GetOne
    {
        public class Input
        {
            public decimal IdBien { get; set; }
        }

        public class Output
        {
            public decimal IdBien { get; set; }
            public string Descripcion { get; set; }
            public decimal IdUsuario { get; set; }
            public string IdTipoBien { get; set; }
            public string IdTipoDominio { get; set; }
            public string IdTipoInmueble { get; set; }
            public string IdTipoMueble { get; set; }
            public decimal TipoBienVal { get; set; }
            public decimal  TipoDominioVal { get; set; }
            public decimal TipoInmuebleVal { get; set; }
            public decimal TipoMuebleVal { get; set; }
}

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from Bien in _context.Bienes
                                    join tipoBien in _context.TipoBienes on Bien.IdTipoBien equals tipoBien.IdTipoBien
                                    join tipoDominio in _context.TipoDominios on Bien.IdTipoDominio equals tipoDominio.IdTipoDominio
                                    join tipoInmueble in _context.TipoInmuebles on Bien.IdTipoInmueble equals tipoInmueble.IdTipoInmueble
                                    join tipoMueble in _context.TipoMuebles on Bien.IdTipoMueble equals tipoMueble.IdTipoMueble
                                    where Bien.IdBien == request.Input.IdBien
                                    select new Output
                                    {
                                        IdBien = Bien.IdBien,
                                        Descripcion = Bien.Descripcion,
                                        IdUsuario = Bien.IdUsuario,
                                        IdTipoBien = tipoBien.Descripcion,
                                        IdTipoDominio = tipoDominio.Descripcion,
                                        IdTipoInmueble = tipoInmueble.Descripcion,
                                        IdTipoMueble = tipoMueble.Descripcion,
                                        TipoBienVal = Bien.IdTipoBien,
                                        TipoDominioVal = Bien.IdTipoDominio,
                                        TipoInmuebleVal = Bien.IdTipoInmueble,
                                        TipoMuebleVal = Bien.IdTipoMueble



                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
