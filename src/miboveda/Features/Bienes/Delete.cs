﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Bienes
{
    public class Delete
    {
        public class Input
        {
            public decimal IdBien { get; set; }

        }

        public class Outut
        {
            public decimal IdBien { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var bien = await _context.Bienes
                    .Where(x => x.IdBien == request.Input.IdBien)
                    .ToListAsync(cancellationToken);

                if (bien.Count() == 0)
                {
                   
                }

                var bienDelete = bien.FirstOrDefault();

                _context.Bienes.Remove(bienDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdBien = bienDelete.IdBien;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
