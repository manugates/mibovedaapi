﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Constants;
using miboveda.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace miboveda.Features.Bienes
{
    public class Create
    {
        public class Input
        {
            public decimal IdBien { get; set; }
            public string Descripcion { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdTipoBien { get; set; }
            public decimal IdTipoDominio { get; set; }
            public decimal IdTipoInmueble { get; set; }
            public decimal IdTipoMueble { get; set; }
        }

        public class Output
        {
            public decimal IdBien { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {

        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public decimal GetMySequence()
            {
                var stringSeq = Database.Sequences.BienesSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }



            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {

                decimal code = 3;
                request.Input.IdBien = GetMySequence();
                var bienCreate = _mapper.Map<Entities.Bienes>(request.Input);

                await _context.Bienes.AddAsync(bienCreate);

                _context.SaveChanges();

                code = bienCreate.IdBien;

                return code == -1
                    ? RequestResult<Output>.Fail("El usuario no se pudo crear")
                    : RequestResult<Output>.Success(new Output { IdBien = bienCreate.IdBien });
            }
        }
    }
}
