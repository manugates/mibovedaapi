﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Beneficiario
{
    public class Update
    {
        public class Input
        {
            public decimal IdBeneficiario { get; set; }
            public string Nombre { get; set; }
            public string ApellidoPaterno { get; set; }
            public string ApellidoMaterno { get; set; }
            public string Rut { get; set; }
            public DateTime FechaNacimiento { get; set; }
            public string Email { get; set; }
            public string Telefono { get; set; }
            public decimal IdSeguro { get; set; }

        }

        public class Output
        {
            public decimal IdBeneficiario { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var BeneficiarioExist = await _context.Beneficiarios.AnyAsync(cancellationToken);
                if (!BeneficiarioExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Nombre);
                }

                Entities.Beneficiario beneficiario = _mapper.Map<Entities.Beneficiario>(request.Input);
                _context.Update(beneficiario);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdBeneficiario = beneficiario.IdBeneficiario });
            }
        }

    }
}
