﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Beneficiario
{
    public class Delete
    {
        public class Input
        {
            public decimal IdBeneficiario { get; set; }

        }

        public class Outut
        {
            public decimal IdBeneficario { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var beneficiario = await _context.Beneficiarios
                    .Where(x => x.IdBeneficiario == request.Input.IdBeneficiario)
                    .ToListAsync(cancellationToken);

                if (beneficiario.Count() == 0)
                {
 
                }

                var beneficiarioDelete = beneficiario.FirstOrDefault();

                _context.Beneficiarios.Remove(beneficiarioDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdBeneficario = beneficiarioDelete.IdBeneficiario;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
