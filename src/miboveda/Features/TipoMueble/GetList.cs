﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.TipoMueble
{
    public class GetList
    {
        public class Output
        {
            public decimal Value { get; set; }
            public string Label { get; set; }
        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {

            public Request()
            {
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from tipoMueble in _context.TipoMuebles

                                    select new Output
                                    {
                                        Value = tipoMueble.IdTipoMueble,
                                        Label = tipoMueble.Descripcion
                                    }).ToListAsync(cancellationToken);

                return result.Any()
                    ? RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result))
                    : RequestResult<List<Output>>.RecordNotFound();

            }
        }
    }
}
