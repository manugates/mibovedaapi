﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Aseguradora
{
    public class Delete
    {
        public class Input
        {
            public string Descripcion { get; set; }

        }

        public class Outut
        {
            public decimal IdAseguradora { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var aseguradora = await _context.Aseguradoras
                    .Where(x => x.Descripcion == request.Input.Descripcion)
                    .ToListAsync(cancellationToken);

                if (aseguradora.Count() == 0)
                {
                    return RequestResult<Outut>.RecordNotFound(request.Input.Descripcion);
                }

                var aseguradoraDelete = aseguradora.FirstOrDefault();

                _context.Aseguradoras.Remove(aseguradoraDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdAseguradora = aseguradoraDelete.IdAseguradora;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
