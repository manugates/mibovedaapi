﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;
namespace miboveda.Features.InformacionIsapre
{
    public class Update
    {
        public class Input
        {
            public decimal IdInformacionIsapre { get; set; }
            public decimal IdTipoIsapre { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdIsapre { get; set; }
        }

        public class Output
        {
            public decimal IdInformacionIsapre { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var InformacionIsapreExist = await _context.InformacionIsapres.AnyAsync(cancellationToken);
                if (!InformacionIsapreExist)
                {

                }

                Entities.InformacionIsapre informacionIsapre = _mapper.Map<Entities.InformacionIsapre>(request.Input);
                _context.Update(informacionIsapre);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdInformacionIsapre = informacionIsapre.IdInformacionIsapre });
            }
        }
    }
}
