﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.InformacionIsapre
{
    public class GetById
    {
        public class Input
        {
            public decimal IdInformacionIsapre { get; set; }
        }

        public class Output
        {
            public decimal IdInformacionIsapre { get; set; }
            public string IdTipoIsapre { get; set; }
            public decimal IdUsuario { get; set; }
            public string IdIsapre { get; set; }
            public decimal IsapreVal { get; set; }
            public decimal TipoIsapreVal { get; set; }

        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from InformacionIsapre in _context.InformacionIsapres
                                    where InformacionIsapre.IdUsuario == request.Input.IdInformacionIsapre
                                    join isapre in _context.Isapres on InformacionIsapre.IdIsapre equals isapre.IdIsapre
                                    join tipoPlan in _context.TiposPlanSalud on InformacionIsapre.IdTipoIsapre equals tipoPlan.IdTipoPlan

                                    select new Output
                                    {
                                        IdInformacionIsapre = InformacionIsapre.IdInformacionIsapre,
                                        IdTipoIsapre = tipoPlan.Descripcion,
                                        IdUsuario = InformacionIsapre.IdUsuario,
                                        IdIsapre = isapre.Nombre,
                                        IsapreVal = InformacionIsapre.IdIsapre,
                                        TipoIsapreVal = InformacionIsapre.IdTipoIsapre


                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
