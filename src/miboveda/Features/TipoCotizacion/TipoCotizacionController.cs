﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using miboveda.Features.Shared;

namespace miboveda.Features.TipoCotizacion
{
    [Route("api/TipoCotizacion")]
    public class TipoCotizacionController : BaseController
    {
        public TipoCotizacionController(IMediator mediator) : base(mediator)
        {

        }

        [HttpGet]
        [Route("getList")]
        [ProducesResponseType(typeof(GetList.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            var request = new GetList.Request();
            return await HandleRequestAsync(request);
        }
    }
}
