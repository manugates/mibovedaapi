﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Banco
{
    public class Update
    {
        public class Input
        {
            public decimal IdBanco { get; set; }
            public string Nombre { get; set; }

        }

        public class Output
        {
            public decimal IdBanco { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var BancoExist = await _context.Bancos.AnyAsync(cancellationToken);
                if (!BancoExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Nombre);
                }

                Entities.Banco banco = _mapper.Map<Entities.Banco>(request.Input);
                _context.Update(banco);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdBanco = banco.IdBanco });
            }
        }
    }
}
