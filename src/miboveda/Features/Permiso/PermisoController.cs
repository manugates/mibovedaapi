﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using miboveda.Features.Shared;

namespace miboveda.Features.Permiso
{
    [Route("api/Permiso")]
    public class PermisoController : BaseController
    {
        public PermisoController(IMediator mediator) : base(mediator)
        {

        }

        [HttpPost]
        [ProducesResponseType(typeof(Create.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Post([FromBody]Create.Input input)
        {
            var request = new Create.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpGet("{IdContacto}")]
        [ProducesResponseType(typeof(GetById.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(GetById.Input input)
        {
            var request = new GetById.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpPatch]
        [ProducesResponseType(typeof(Update.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Patch([FromBody]Update.Input input)
        {
            var request = new Update.Request(input);
            return await HandleRequestAsync(request);
        }


    }
}
