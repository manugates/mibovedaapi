﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Permiso
{
    public class GetById
    {
        public class Input
        {
            public decimal IdContacto { get; set; }
        }

        public class Output
        {
            public decimal Seguro { get; set; }
            public decimal CuentaBancaria { get; set; }
            public decimal DatosProvisionales { get; set; }
            public decimal Bienes { get; set; }
            public decimal Deudas { get; set; }
            public decimal FotosVideos { get; set; }
            public decimal Documentos { get; set; }
            public decimal Deseos { get; set; }
            public decimal Instrucciones { get; set; }
            public decimal Testimonios { get; set; }
            public decimal IdContacto { get; set; }
        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from Permiso in _context.Permisos
                                    where Permiso.IdContacto == request.Input.IdContacto
                                    select new Output
                                    {
                                         Seguro = Permiso.Seguro,
                                         CuentaBancaria = Permiso.CuentaBancaria,
                                         DatosProvisionales = Permiso.DatosProvisionales,
                                         Bienes = Permiso.Bienes,
                                         Deudas = Permiso.Deudas,
                                         FotosVideos = Permiso.FotosVideos,
                                         Documentos = Permiso.Documentos,
                                         Deseos = Permiso.Deseos,
                                         Instrucciones = Permiso.Instrucciones,
                                         Testimonios = Permiso.Testimonios,
                                         IdContacto = Permiso.IdContacto


        })
                                    .ToListAsync(cancellationToken);
                     return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
