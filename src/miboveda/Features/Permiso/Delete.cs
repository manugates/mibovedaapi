﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Permiso
{
    public class Delete
    {
        public class Input
        {
            public decimal IdContacto { get; set; }

        }

        public class Outut
        {
            public decimal IdContacto { get; set; }
        }

        public class Request : IRequest<RequestResult<Outut>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Outut>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Outut>> Handle(Request request, CancellationToken cancellationToken)
            {
                var permiso = await _context.Permisos
                    .Where(x => x.IdContacto == request.Input.IdContacto)
                    .ToListAsync(cancellationToken);

                if (permiso.Count() == 0)
                {
                    
                }

                var permisoDelete = permiso.FirstOrDefault();

                _context.Permisos.Remove(permisoDelete);
                await _context.SaveChangesAsync(cancellationToken);

                Outut output = new Outut();
                output.IdContacto = permisoDelete.IdContacto;

                return RequestResult<Outut>.Success(output);
            }
        }
    }
}
