﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Permiso
{
    public class Update
    {
        public class Input
        {
            public decimal Seguro { get; set; }
            public decimal CuentaBancaria { get; set; }
            public decimal DatosProvisionales { get; set; }
            public decimal Bienes { get; set; }
            public decimal Deudas { get; set; }
            public decimal FotosVideos { get; set; }
            public decimal Documentos { get; set; }
            public decimal Deseos { get; set; }
            public decimal Instrucciones { get; set; }
            public decimal Testimonios { get; set; }
            public decimal IdContacto { get; set; }
        }

        public class Output
        {
            public decimal IdContaco { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var PermisoExist = await _context.Permisos.AnyAsync(cancellationToken);
                if (!PermisoExist)
                {
                  
                }

                Entities.Permiso permiso = _mapper.Map<Entities.Permiso>(request.Input);
                _context.Update(permiso);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdContaco = permiso.IdContacto });
            }
        }
    }
}
