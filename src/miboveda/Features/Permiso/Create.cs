﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Permiso
{
    public class Create
    {
        public class Input
        {
            public decimal Seguro { get; set; }
            public decimal CuentaBancaria { get; set; }
            public decimal DatosProvisionales { get; set; }
            public decimal Bienes { get; set; }
            public decimal Deudas { get; set; }
            public decimal FotosVideos { get; set; }
            public decimal Documentos { get; set; }
            public decimal Deseos { get; set; }
            public decimal Instrucciones { get; set; }
            public decimal Testimonios { get; set; }
            public decimal IdContacto { get; set; }
        }

        public class Output
        {
            public decimal IdContacto { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {

        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }



            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {

                var code = 3;
                var permisoCreate = _mapper.Map<Entities.Permiso>(request.Input);

                await _context.Permisos.AddAsync(permisoCreate);

                code = _context.SaveChanges();

                var output = new Output();


                return code == 3
                    ? RequestResult<Output>.RecordNotFound()
                    : RequestResult<Output>.Success(output);
            }
        }
    }
}
