﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Constants;
using miboveda.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace miboveda.Features.Contacto
{
    public class Create
    {
        public class Input
        {
            public decimal IdContacto { get; set; }
            public string Nombre { get; set; }
            public string ApellidoPaterno { get; set; }
            public string ApellidoMaterno { get; set; }
            public DateTime FechaNacimiento { get; set; }
            public string Rut { get; set; }
            public string Password { get; set; }
            public DateTime FechaInscripcion { get; set; }
            public string Email { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdParentesco { get; set; }
            public string Sexo { get; set; }
            public decimal Activo { get; set; }

        }

        public class Output
        {
            public decimal IdContacto { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {

        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public decimal GetMySequence()
            {
                var stringSeq = Database.Sequences.ContactoSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }

            public decimal GetMyPermisoSequence()
            {
                var stringSeq = Database.Sequences.PermisoSeq.sequencie.ToString();
                var secuencias = _context.Secuencias.FromSql(stringSeq).ToList();

                var result = secuencias.FirstOrDefault();

                return Convert.ToDecimal(result.Nextval);
            }

            public bool SendEmail(string nombreUsuario, string password, string nombreTitular, string titular, string contacto, string correo)
            {
                var send = true;
                var rutTitular = formatearRut(titular);
                var rutContacto = formatearRut(contacto);
                /* Empiezo a leer la plantilla de correo Titular */
                string Body = System.IO.File.ReadAllText("Plantilla/emailContacto.html");
                /* Realizo los reemplazos necesarios */
                Body = Body.Replace("#NombreUsuario", nombreUsuario).Replace("#Contraseña", password).Replace("#Titular",rutTitular).Replace("#Contacto",rutContacto).Replace("#NombreTitular", nombreTitular);
                //Agrego las imagenes al html
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(Body, Encoding.UTF8, MediaTypeNames.Text.Html);
                LinkedResource imgLogo = new LinkedResource("Plantilla/logo.png", MediaTypeNames.Image.Jpeg);
                imgLogo.ContentId = "logo";
                LinkedResource imgBoveda = new LinkedResource("Plantilla/boveda.jpg", MediaTypeNames.Image.Jpeg);
                imgBoveda.ContentId = "boveda";

                htmlView.LinkedResources.Add(imgLogo);
                htmlView.LinkedResources.Add(imgBoveda);
                //Empiezo a crear los datos del envio del correo
                MailMessage message = new MailMessage();
                message.From = new MailAddress("no-responder@miboveda.cl");
                message.To.Add(correo);
                message.Subject = "Bienvenido a Mi Bóveda";
                message.IsBodyHtml = true;

                message.AlternateViews.Add(htmlView);
                //configuracion del envio del correo
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Host = "mail.miboveda.cl";
                smtpClient.Port = 25;
                smtpClient.EnableSsl = false;

                smtpClient.Credentials = new System.Net.NetworkCredential("no-responder@miboveda.cl", "pga123654");

                try
                {
                    //Se envia correo
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteServerCertificateValidationCallback);
                    smtpClient.Send(message);
                }
                catch (Exception ex)
                {
                    return !send;
                }

                return send;
            }

            private bool RemoteServerCertificateValidationCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                //Console.WriteLine(certificate);
                return true;
            }

            public string formatearRut(string rut)
            {
                int cont = 0;
                string format;
                if (rut.Length == 0)
                {
                    return "";
                }
                else
                {
                    rut = rut.Replace(".", "");
                    rut = rut.Replace("-", "");
                    format = "-" + rut.Substring(rut.Length - 1);
                    for (int i = rut.Length - 2; i >= 0; i--)
                    {
                        format = rut.Substring(i, 1) + format;
                        cont++;
                        if (cont == 3 && i != 0)
                        {
                            format = "." + format;
                            cont = 0;
                        }
                    }
                    return format;
                }
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                
                decimal code = 3;
                request.Input.IdContacto = GetMySequence();
                request.Input.FechaInscripcion = DateTime.Now;
                var contactoCreate = _mapper.Map<Entities.Contacto>(request.Input);


                var permisoCreate = new Entities.Permiso();
                permisoCreate.IdContacto = request.Input.IdContacto;
                permisoCreate.Seguro = 0;
                permisoCreate.CuentaBancaria = 0;
                permisoCreate.DatosProvisionales = 0;
                permisoCreate.Bienes = 0;
                permisoCreate.Deudas = 0;
                permisoCreate.FotosVideos = 0;
                permisoCreate.Documentos = 0;
                permisoCreate.Deseos = 0;
                permisoCreate.Testimonios = 0;
                

                await _context.Contactos.AddAsync(contactoCreate);
                await _context.Permisos.AddAsync(permisoCreate);

                code = _context.SaveChanges();

                code = contactoCreate.IdContacto;

                string nombre = contactoCreate.Nombre + " " + contactoCreate.ApellidoPaterno;
                var titular = _context.Usuarios.Where(element => element.IdUsuario == contactoCreate.IdUsuario).FirstOrDefault();
                string nombreTitular = titular.Nombre + " " + titular.ApellidoPaterno;
                var envioCorreo = SendEmail(nombre, contactoCreate.Password, nombreTitular, titular.Rut, contactoCreate.Rut, contactoCreate.Email);


                return code == -1
                    ? RequestResult<Output>.Fail("El usuario no se pudo crear")
                    : RequestResult<Output>.Success(new Output { IdContacto = contactoCreate.IdContacto });
            }
        }
    }
}
