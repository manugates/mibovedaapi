﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Contacto
{
    public class ContactoExiste
    {
        public class Input
        {
            public string IdUsuario { get; set; }
            public string Rut { get; set; }
        }

        public class Output
        {
            public decimal IdContacto { get; set; }
            public string Nombre { get; set; }
            public string ApellidoPaterno { get; set; }
            public string ApellidoMaterno { get; set; }
            public DateTime FechaNacimiento { get; set; }
            public string Rut { get; set; }
            public string Password { get; set; }
            public DateTime FechaInscripcion { get; set; }
            public string Email { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdParentesco { get; set; }
            public string Sexo { get; set; }


        }

        public class Request : IRequest<RequestResult<List<Output>>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {

            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<List<Output>>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<List<Output>>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = await (from Contacto in _context.Contactos
                                    join Usuario in _context.Usuarios on Contacto.IdUsuario equals Usuario.IdUsuario
                                    where Contacto.Rut == request.Input.Rut && Usuario.Rut == request.Input.IdUsuario
                                    select new Output
                                    {
                                        IdContacto = Contacto.IdContacto,
                                        Nombre = Contacto.Nombre,
                                        ApellidoPaterno = Contacto.ApellidoPaterno,
                                        ApellidoMaterno = Contacto.ApellidoMaterno,
                                        FechaNacimiento = Contacto.FechaNacimiento,
                                        Rut = Contacto.Rut,
                                        Password = Contacto.Password,
                                        FechaInscripcion = Contacto.FechaInscripcion,
                                        Email = Contacto.Email,
                                        IdUsuario = Contacto.IdUsuario,
                                        IdParentesco = Contacto.IdParentesco,
                                        Sexo = Contacto.Sexo

                                    })
                                    .ToListAsync(cancellationToken);
                return result.Count() == 0
                    ? RequestResult<List<Output>>.RecordNotFound()
                    : RequestResult<List<Output>>.Success(_mapper.Map<List<Output>>(result));
            }
        }
    }
}
