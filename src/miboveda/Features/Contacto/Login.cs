﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;

namespace miboveda.Features.Contacto
{
    public class Login
    {
        public class Input
        {
            public string RutUsuario { get; set; }
            public string Contacto { get; set; }
            public string Password { get; set; }
        }

        public class Output
        {
            public decimal Code { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Validator : AbstractValidator<Request>
        {
            public Validator()
            {
                
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;


            public Handler(MiBovedaDbContext context)
            {
                _context = context;

            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var resultado = await (from contacto in _context.Contactos
                                    join usuario in _context.Usuarios on contacto.IdUsuario equals usuario.IdUsuario
                                     where contacto.Rut == request.Input.Contacto && usuario.Rut == request.Input.RutUsuario && contacto.Password == request.Input.Password
                                     select new Output
                                     {
                                         Code = contacto.IdContacto
                                     })
                                     .ToListAsync(cancellationToken);


                return resultado.Any()
                    ? RequestResult<Output>.Success(new Output { Code = 0 })
                    : RequestResult<Output>.RecordNotFound(request.Input.Contacto);

            }
        }
    }
}
