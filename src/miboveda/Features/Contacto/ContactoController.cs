﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using miboveda.Features.Shared;

namespace miboveda.Features.Contacto
{
    [Route("api/Contacto")]
    public class ContactoController : BaseController
    {
        public ContactoController(IMediator mediator) : base(mediator)
        {

        }

        [HttpPost]
        [ProducesResponseType(typeof(Create.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Post([FromBody]Create.Input input)
        {
            var request = new Create.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpGet("{IdContacto}")]
        [ProducesResponseType(typeof(GetById.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(GetById.Input input)
        {
            var request = new GetById.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpGet("GetByRut/{Rut}")]
        [ProducesResponseType(typeof(GetByRut.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(GetByRut.Input input)
        {
            var request = new GetByRut.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpGet]
        [Route("getList")]
        [ProducesResponseType(typeof(GetList.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            var request = new GetList.Request();
            return await HandleRequestAsync(request);
        }

        [HttpGet("contactoExiste/{IdUsuario}/{Rut}")]
        [ProducesResponseType(typeof(ContactoExiste.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> ContactoExiste(ContactoExiste.Input input)
        {
            var request = new ContactoExiste.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpGet]
        [Route("GetByUsuario/{IdUsuario}")]
        [ProducesResponseType(typeof(GetByUsuario.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(GetByUsuario.Input input)
        {
            var request = new GetByUsuario.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpGet("getOne/{IdContacto}")]
        [ProducesResponseType(typeof(GetOne.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(GetOne.Input input)
        {
            var request = new GetOne.Request(input);
            return await HandleRequestAsync(request);
        }


        [HttpPatch]
        [ProducesResponseType(typeof(Update.Output), StatusCodes.Status200OK)]
        public async Task<IActionResult> Patch([FromBody]Update.Input input)
        {
            var request = new Update.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpPost]
        [Route("delete")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete([FromBody]Delete.Input input)
        {
            var request = new Delete.Request(input);
            return await HandleRequestAsync(request);
        }

        [HttpPost]
        [Route("login")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Login([FromBody]Login.Input input)
        {
            var request = new Login.Request(input);
            return await HandleRequestAsync(request);
        }
    }
}
