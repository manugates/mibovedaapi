﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.Contacto
{
    public class Update
    {
        public class Input
        {
            public decimal IdContacto { get; set; }
            public string Nombre { get; set; }
            public string ApellidoPaterno { get; set; }
            public string ApellidoMaterno { get; set; }
            public DateTime FechaNacimiento { get; set; }
            public string Rut { get; set; }
            public string Password { get; set; }
            public DateTime FechaInscripcion { get; set; }
            public string Email { get; set; }
            public decimal IdUsuario { get; set; }
            public decimal IdParentesco { get; set; }
            public string Sexo { get; set; }
            public decimal Activo { get; set; }

        }

        public class Output
        {
            public decimal IdContacto { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var ContactoExist = await _context.Contactos.AnyAsync(cancellationToken);
                if (!ContactoExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Nombre);
                }

                Entities.Contacto contacto = _mapper.Map<Entities.Contacto>(request.Input);
                _context.Update(contacto);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdContacto = contacto.IdContacto });
            }
        }
    }
}
