﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using miboveda.Features.Shared;
using miboveda.Infrastructure.Data;
using miboveda.Infrastructure.Extensions;

namespace miboveda.Features.TipoCuenta
{
    public class Update
    {
        public class Input
        {
            public decimal IdTipoCuenta { get; set; }
            public string Descripcion { get; set; }
        }

        public class Output
        {
            public decimal IdTipoCuenta { get; set; }
        }

        public class Request : IRequest<RequestResult<Output>>
        {
            public Input Input { get; }

            public Request(Input input)
            {
                Input = input;
            }
        }

        public class Handler : IRequestHandler<Request, RequestResult<Output>>
        {
            private readonly MiBovedaDbContext _context;
            private readonly IMapper _mapper;

            public Handler(MiBovedaDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RequestResult<Output>> Handle(Request request, CancellationToken cancellationToken)
            {
                var TipoCuentaExist = await _context.TipoCuentas.AnyAsync(cancellationToken);
                if (!TipoCuentaExist)
                {
                    return RequestResult<Output>.RecordNotFound(request.Input.Descripcion);
                }

                Entities.TipoCuenta tipocuenta = _mapper.Map<Entities.TipoCuenta>(request.Input);
                _context.Update(tipocuenta);
                await _context.SaveChangesAsync(cancellationToken);
                return RequestResult<Output>.Success(new Output { IdTipoCuenta = tipocuenta.IdTipoCuenta });
            }
        }
    }
}
