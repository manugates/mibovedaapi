﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miboveda.Infrastructure.Constants
{
    public static class ResponseMessages
    {
        public struct BadRequest
        {
            public const string ReceivedMessage = "A bad request was received.";
            public const string BodyWithNoUsableContent = "The body of the request contained no usable content.";
        }
    }
}
