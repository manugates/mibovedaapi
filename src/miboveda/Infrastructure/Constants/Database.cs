﻿namespace miboveda.Infrastructure.Constants
{
    public static class Database
    {
        public const string Schema = "public";

        public struct Tables
        {
            public struct afp
            {
                public const string TableName = "afp";

                public struct Columns
                {
                    public const string IdAfp = "id_afp";
                    public const string Nombre = "nombre";
                }
            }

            public struct isapre
            {
                public const string TableName = "isapre";

                public struct Columns
                {
                    public const string IdIsapre = "id_isapre";
                    public const string Nombre = "nombre";
                }
            }

            public struct tipoDeuda
            {
                public const string TableName = "tipo_deuda";

                public struct Columns
                {
                    public const string IdTipoDeuda = "id_tipo_deuda";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct tipoArchivo
            {
                public const string TableName = "tipo_archivo";

                public struct Columns
                {
                    public const string IdTipoArchivo = "id_tipo_archivo";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct tipoBien
            {
                public const string TableName = "tipo_bien";

                public struct Columns
                {
                    public const string IdTipoBien = "id_tipo_bien";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct tipoCuenta
            {
                public const string TableName = "tipo_cuenta";

                public struct Columns
                {
                    public const string IdTipoCuenta = "id_tipo_cuenta";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct tipoInversion
            {
                public const string TableName = "tipo_inversion";

                public struct Columns
                {
                    public const string IdTipoInversion = "id_tipo_inversion";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct parentesco
            {
                public const string TableName = "parentesco";

                public struct Columns
                {
                    public const string IdParentesco = "id_parentesco";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct tipoSeguro
            {
                public const string TableName = "tipo_seguro";

                public struct Columns
                {
                    public const string IdTipoSeguro = "id_tipo_seguro";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct cajaCompensacion
            {
                public const string TableName = "caja_compensacion";

                public struct Columns
                {
                    public const string IdCaja = "id_caja";
                    public const string Nombre = "nombre";
                }
            }

            public struct usuario
            {
                public const string TableName = "usuario";

                public struct Columns
                {
                    public const string IdUsuario = "id_usuario";
                    public const string Rut = "rut";
                    public const string Password = "password";
                    public const string Email = "email";
                    public const string FechaNacimiento = "fecha_nac";
                    public const string FechaInscripcion = "fecha_registro";
                    public const string Nombre = "nombre";
                    public const string ApellidoPaterno = "apepat";
                    public const string ApellidoMaterno = "apemat";
                    public const string FechaCreacion = "created_date";
                    public const string FechaUpdate = "updated_date";
                    public const string CreadoPor = "createdby";
                    public const string IdAfp = "afp_id_afp";
                    public const string IdIsapre = "isapre_id_isapre";
                    public const string IdPlan = "plan_id_plan";
                    public const string Telefono = "celular";
                    public const string Comuna = "comuna";
                }
            }
            public struct Secuencia
            {
                public struct Columns
                {
                    public const string Nextval = "NEXTVAL";
                }
            }

            public struct seguro
            {
                public const string TableName = "seguro";

                public struct Columns
                {
                    public const string IdSeguro = "id_seguro";
                    public const string NumeroPoliza = "numero_poliza";
                    public const string IdTipoSeguro = "tipo_seguro_id_tipo";
                    public const string IdAseguradora = "aseguradora_id_aseguradora";
                    public const string IdUsuario = "usuario_id_seguro";
                    public const string Vigencia = "vigencia";
                    public const string Ahorro = "ahorro";

                }

            }

            public struct plan
            {
                public const string TableName = "plan";

                public struct Columns
                {
                    public const string IdPlan = "id_plan";
                    public const string Descripcion = "descripcion";
                    public const string Precio = "precio";

                }

            }

            public struct permiso
            {
                public const string TableName = "permiso";

                public struct Columns
                {
                    public const string Seguro = "seguro";
                    public const string CuentaBancaria = "cuenta_bancaria";
                    public const string DatosPrevisionales = "datos_previsionales";
                    public const string Bienes = "bienes";
                    public const string Deudas = "deudas";
                    public const string FotosVideos = "fotos_videos";
                    public const string Documentos = "documentos";
                    public const string Deseos = "deseos";
                    public const string Instrucciones = "instrucciones";
                    public const string Testimonios = "testimonios";
                    public const string IdContacto = "contacto_id_contacto";

                }
            }

            public struct inversion
            {
                public const string TableName = "inversion";

                public struct Columns
                {
                    public const string IdInversion = "id_inversion";
                    public const string Descripcion = "Operacion";
                    public const string IdTipoInversion = "tipo_inversion_id_tipo";
                    public const string IdBanco = "id_banco";
                    public const string IdUsuario = "usuario_id_usuario";

                }
            }

            public struct aseguradora
            {
                public const string TableName = "aseguradora";

                public struct Columns
                {
                    public const string IdAseguradora = "id_aseguradora";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct archivo
            {
                public const string TableName = "archivo";

                public struct Columns
                {
                    public const string IdArchivo = "id_archivo";
                    public const string File = "archivo";
                    public const string IdUsuario = "usuario_id_usuario";
                    public const string IdTipoArchivo = "tipo_archivo_id_tipo";
                }
            }

            public struct empresa
            {
                public const string TableName = "empresa";

                public struct Columns
                {
                    public const string IdEmpresa = "id_empresa";
                    public const string Nombre = "nombre";
                    public const string RazonSocial = "razon_social";
                    public const string FechaIngreso = "fecha_ing";
                    public const string Direccion = "direccion";
                    public const string Cargo = "cargo";
                    public const string Profesion = "profesion";
                    public const string IdCajaCompensacion = "caja_compensacion_id_caja";
                    public const string NombreContacto = "nombre_contacto";
                    public const string ApellidoPaterno = "apepat_contacto";
                    public const string ApellidoMaterno = "apemat_contacto";
                    public const string Email = "email";
                    public const string Telefono = "celular";
                    public const string Rut = "rut";
                    public const string Comuna = "comuna";
                    public const string IdUsuario = "usuario_id_usuario";
                }
            }

            public struct deuda
            {
                public const string TableName = "deuda";

                public struct Columns
                {
                    public const string IdDeuda = "id_deuda";
                    public const string Operacion = "operacion";
                    public const string Monto = "monto";
                    public const string SeguroDegravament = "seguro_degravament";
                    public const string Mora = "mora";
                    public const string IdUsuario = "usuario_id_usuario";
                    public const string IdTipoDeuda = "tipo_deuda_id_tipo";
                    public const string IdBanco = "banco_id_banco";
                    public const string IdProducto = "producto_id_producto";
                    public const string IdCaja = "caja_id_caja";
                    public const string IdCasaComercial = "casa_id_casa_comercial";
                    public const string IdEntidad = "entidad_id_entidad";
                }
            }

            public struct cuenta
            {
                public const string TableName = "cuenta";

                public struct Columns
                {
                    public const string IdCuenta = "id_cuenta";
                    public const string Numero = "numero";
                    public const string IdBanco = "banco_id_banco";
                    public const string IdTipoCuenta = "tipo_cuenta_id_tipo";
                    public const string IdUsuario = "usuario_id_usuario";

                }
            }

            public struct contacto
            {
                public const string TableName = "contacto";

                public struct Columns
                {
                    public const string IdContacto = "id_contacto";
                    public const string Nombre = "nombre";
                    public const string ApellidoPaterno = "apepat";
                    public const string ApellidoMaterno = "apemat";
                    public const string FechaNacimiento = "fecha_nacim";
                    public const string Rut = "rut";
                    public const string Password = "password";
                    public const string FechaInscripcion = "fecha_inscrip";
                    public const string Email = "email";
                    public const string IdUsuario = "usuario_id_usuario";
                    public const string IdParentesco = "parentesco_id_parentesco";
                    public const string Sexo = "sexo";

                }
            }

            public struct beneficiario
            {
                public const string TableName = "beneficiario";

                public struct Columns
                {
                    public const string IdBeneficiario = "id_beneficiario";
                    public const string Nombre = "nombre";
                    public const string ApellidoPaterno = "apepat";
                    public const string ApellidoMaterno = "apemat";
                    public const string Rut = "rut";
                    public const string FechaNacimiento = "fecha_nac";
                    public const string Email = "email";
                    public const string Telefono = "celular";
                    public const string IdSeguro = "seguro_id_seguro";


                }
            }

            public struct bienes
            {
                public const string TableName = "bienes";

                public struct Columns
                {
                    public const string IdBien = "id_bien";
                    public const string Descripcion = "descripcion";
                    public const string IdUsuario = "usuario_id_usuario";
                    public const string IdTipoBien = "tipo_bien_id_tipo";
                    public const string IdTipoDominio = "id_tipo_dominio";
                    public const string IdTipoInmueble = "id_tipo_inmueble";
                    public const string IdTipoMueble = "id_tipo_mueble";

                }
            }

            public struct banco
            {
                public const string TableName = "banco";

                public struct Columns
                {
                    public const string IdBanco = "id_banco";
                    public const string Nombre = "nombre";
                }
            }

            public struct comuna
            {
                public const string TableName = "comuna";

                public struct Columns
                {
                    public const string IdComuna = "id_comuna";
                    public const string Nombre = "nombre";
                }
            }

            public struct tipoPlanSalud
            {
                public const string TableName = "tipo_plan_salud";

                public struct Columns
                {
                    public const string IdTipoPlan = "id_tipo_plan_salud";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct tipoFondo
            {
                public const string TableName = "tipo_fondo";

                public struct Columns
                {
                    public const string IdTipoFondo = "id_tipo_fondo";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct tipoInmueble
            {
                public const string TableName = "tipo_inmueble";

                public struct Columns
                {
                    public const string IdTipoInmueble = "id_tipo_inmueble";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct tipoMueble
            {
                public const string TableName = "tipo_mueble";

                public struct Columns
                {
                    public const string IdTipoMueble = "id_tipo_mueble";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct tipoCotizacion
            {
                public const string TableName = "tipo_cotizacion";

                public struct Columns
                {
                    public const string IdTipoCotizacion = "id_tipo_cotizacion";
                    public const string Descripcion = "descripcion";
                }
            }

            public struct informacionAfp
            {
                public const string TableName = "info_afp";

                public struct Columns
                {
                    public const string IdInformacionAfp = "id_info_afp";
                    public const string IdTipoFondo = "tipo_fondo_id_info_afp";
                    public const string IdUsuario = "usuario_id_usuario";
                    public const string IdAfp = "afp_id_afp";
                    public const string IdTipoCotizacion = "tipo_cotizacion_id_info_afp";
                    
                }
            }

            public struct informacionIsapre
            {
                public const string TableName = "info_isapre";

                public struct Columns
                {
                    public const string IdInformacionIsapre = "id_info_isapre";
                    public const string IdTIpoIsapre = "tipo_plan_salud_id_tipo_isapre";
                    public const string IdUsuario = "usuario_id_usuario";
                    public const string IdIsapre = "isapre_id_isapre";

                }
            }

            public struct informacionCajaCompensacion
            {
                public const string TableName = "info_caja";

                public struct Columns
                {
                    public const string IdInformacionCaja = "id_info_caja";
                    public const string IdUsuario = "usuario_id_usuario";
                    public const string IdCajaCompensacion = "caja_id_caja";

                }
            }

            public struct tipoDominio
            {
                public const string TableName = "tipo_dominio";

                public struct Columns
                {
                    public const string IdTipoDominio = "id_tipo_dominio";
                    public const string Descripcion = "descripcion";

                }
            }

            public struct producto
            {
                public const string TableName = "producto";

                public struct Columns
                {
                    public const string IdProducto = "id_producto";
                    public const string Descripcion = "descripcion";

                }
            }

            public struct casaComercial
            {
                public const string TableName = "casa_comercial";

                public struct Columns
                {
                    public const string idCasaComercial = "id_casa_comercial";
                    public const string descripcion = "descripcion";

                }
            }

            public struct entidad
            {
                public const string TableName = "entidad";

                public struct Columns
                {
                    public const string idEntidad = "id_entidad";
                    public const string descripcion = "descripcion";

                }
            }

        }

        public struct Types
        {
            public const string Varchar2 = "VARCHAR";
            public const string Number = "NUMBER";
            public const string Date = "DATE";
            public const string Char = "CHAR";
            public const string Int = "INT";
            public const string Bool = "BOOL";
        }

        public struct Sequences
        {
            public struct UsuarioSeq
            {
                public const string sequencie = "select nextval('usuario_seq');";
            }

            public struct AfpSeq
            {
                public const string sequencie = "select nextval('afp_seq');";
            }

            public struct ArchivoSeq
            {
                public const string sequencie = "select nextval('archivo_seq');";
            }

            public struct BienesSeq
            {
                public const string sequencie = "select nextval('bienes_seq');";
            }

            public struct ContactoSeq
            {
                public const string sequencie = "select nextval('contacto_seq');";
            }

            public struct CuentaSeq
            {
                public const string sequencie = "select nextval('cuenta_seq');";
            }

            public struct DeudaSeq
            {
                public const string sequencie = "select nextval('deuda_seq');";
            }

            public struct EmpresaSeq
            {
                public const string sequencie = "select nextval('empresa_seq');";
            }

            public struct InversionSeq
            {
                public const string sequencie = "select nextval('inversion_seq');";
            }

            public struct PermisoSeq
            {
                public const string sequencie = "select nextval('permiso_seq');";
            }

            public struct SeguroSeq
            {
                public const string sequencie = "select nextval('seguro_seq');";
            }

            public struct InformacionAfpSeq
            {
                public const string sequencie = "select nextval('informacion_afp_seq');";
            }

            public struct InformacionIsapreSeq
            {
                public const string sequencie = "select nextval('informacion_isapre_seq');";
            }

            public struct InformacionCajaSeq
            {
                public const string sequencie = "select nextval('informacion_caja_seq');";
            }

            public struct BeneficiarioSeq
            {
                public const string sequencie = "select nextval('beneficiario_seq');";
            }
        }
    }
}