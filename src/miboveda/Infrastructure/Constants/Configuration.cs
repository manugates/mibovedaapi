﻿
namespace miboveda.Infrastructure.Constants
{
    public class Configuration
    {
        public const string ConnectionStringKey = "DefaultConnection";
        public const string SwaggerApiTitle = "MIBOVEDA-API";
        public const string SwaggerApiVersion = "v1.0";
        public const string SwaggerEndPoint = "/swagger/v1.0/swagger.json";
        public const string SwaggerEndPointDescription = "API Documentation";
        public const string GlobalResponseType = "application/json";
    }
}
