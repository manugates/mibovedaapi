﻿using System.Reflection;
using AutoMapper;
using MediatR;
using miboveda.Infrastructure.Constants;
using miboveda.Infrastructure.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using FluentValidation.AspNetCore;
using miboveda.Infrastructure.Pipelines;

namespace miboveda.Infrastructure.Extensions
{
    public static class StartupExtensions
    {
        internal static void AddDbContexts(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString(Configuration.ConnectionStringKey);

            services.AddDbContext<MiBovedaDbContext>(options => options
                    .UseLazyLoadingProxies()
                    .UseNpgsql(connectionString));
        }

        internal static void RegisterMvc(this IServiceCollection services)
        {
            services.AddMvcCore(options =>
            {
                options.Filters.Add(new ProducesAttribute(Configuration.GlobalResponseType));
                options.Filters.Add(new ProducesResponseTypeAttribute(200));
                options.Filters.Add(new ProducesResponseTypeAttribute(400));
                options.Filters.Add(new ProducesResponseTypeAttribute(500));
            })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddFluentValidation(options =>
                {
                    options.RegisterValidatorsFromAssemblyContaining<Startup>();
                })
                .AddAuthorization(options =>
                {

                })
                .AddJsonFormatters()
                .AddApiExplorer();
        }

        internal static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup).GetTypeInfo().Assembly);
        }

        internal static void RegisterSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(Configuration.SwaggerApiVersion,
                    new Info
                    {
                        Title = Configuration.SwaggerApiTitle,
                        Version = Configuration.SwaggerApiVersion
                    });

                c.CustomSchemaIds(x => x.FullName);
                c.MapType<decimal>(() => new Schema { Type = "number", Format = "decimal" });
                c.OrderActionsBy((apiDesc) => $"{apiDesc.ActionDescriptor.RouteValues}_{apiDesc.RelativePath}");
            });
        }


        internal static void RegisterSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(Configuration.SwaggerEndPoint, Configuration.SwaggerApiVersion);
                c.DocumentTitle = Configuration.SwaggerEndPointDescription;
                c.DocExpansion(DocExpansion.List);
            });
        }

        internal static void RegisterMediatR(this IServiceCollection services)
        {
            services.AddMediatR(typeof(Startup).GetTypeInfo().Assembly);
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ExceptionBehavior<,>));
        }
    }
}
