﻿using FluentValidation;

namespace miboveda.Infrastructure.Extensions
{
    public static class ValidationOptionsExtension
    {
        public static IRuleBuilderOptions<T, TProperty> NotEmptyWithMessage<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder)
        {
            return ruleBuilder.NotEmpty().WithMessage("'{PropertyName}' no debe estar vacío.");
        }

        public static IRuleBuilderOptions<T, string> LengthWithMessage<T>(this IRuleBuilder<T, string> ruleBuilder, int min, int max)
        {
            return ruleBuilder.Length(min, max).WithMessage("'{PropertyName}' debe tener entre {MinLength} y {MaxLength} caracter(es). Actualmente tiene {TotalLength} caracter(es).");
        }

        public static IRuleBuilderOptions<T, string> MatchesWithMessage<T>(this IRuleBuilder<T, string> ruleBuilder, string expression)
        {
            return ruleBuilder.Matches(expression).WithMessage("'{PropertyName}' no tiene el formato correcto.");
        }
    }
}
