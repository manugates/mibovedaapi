﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class SecuenciaMap : IEntityTypeConfiguration<Secuencia>
    {
        public void Configure(EntityTypeBuilder<Secuencia> builder)
        {
            builder.HasKey(x => x.Nextval);
            builder.Property(x => x.Nextval)
                .HasColumnName(Database.Tables.Secuencia.Columns.Nextval)
                .HasColumnType(Database.Types.Int)
                .HasMaxLength(60);
        }
    }
}
