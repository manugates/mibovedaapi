﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class InformacionCajaCompensacionMap : IEntityTypeConfiguration<InformacionCaja>
    {
        public void Configure(EntityTypeBuilder<InformacionCaja> builder)
        {
            builder.ToTable(Database.Tables.informacionCajaCompensacion.TableName, Database.Schema);
            builder.HasKey(x => x.IdInformacionCaja);
            builder.Property(x => x.IdInformacionCaja)
                .HasColumnName(Database.Tables.informacionCajaCompensacion.Columns.IdInformacionCaja)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdUsuario)
                .HasColumnName(Database.Tables.informacionCajaCompensacion.Columns.IdUsuario)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdCajaCompensacion)
                .HasColumnName(Database.Tables.informacionCajaCompensacion.Columns.IdCajaCompensacion)
                .HasColumnType(Database.Types.Int);

        }
    }
}
