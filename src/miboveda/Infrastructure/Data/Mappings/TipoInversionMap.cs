﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class TipoInversionMap :  IEntityTypeConfiguration<TipoInversion>
    {
        public void Configure(EntityTypeBuilder<TipoInversion> builder)
        {
            builder.ToTable(Database.Tables.tipoInversion.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoInversion);
            builder.Property(x => x.IdTipoInversion)
                .HasColumnName(Database.Tables.tipoInversion.Columns.IdTipoInversion)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoInversion.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
