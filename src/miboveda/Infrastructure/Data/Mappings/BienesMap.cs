﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class BienesMap : IEntityTypeConfiguration<Bienes>
    {
        public void Configure(EntityTypeBuilder<Bienes> builder)
        {
            builder.ToTable(Database.Tables.bienes.TableName, Database.Schema);
            builder.HasKey(x => x.IdBien);
            builder.Property(x => x.IdBien)
                .HasColumnName(Database.Tables.bienes.Columns.IdBien)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.bienes.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.IdUsuario)
                .HasColumnName(Database.Tables.bienes.Columns.IdUsuario)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdTipoBien)
                .HasColumnName(Database.Tables.bienes.Columns.IdTipoBien)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdTipoDominio)
                .HasColumnName(Database.Tables.bienes.Columns.IdTipoDominio)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdTipoInmueble)
                .HasColumnName(Database.Tables.bienes.Columns.IdTipoInmueble)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdTipoMueble)
                .HasColumnName(Database.Tables.bienes.Columns.IdTipoMueble)
                .HasColumnType(Database.Types.Int);
        }
    }
}
