﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class ComunaMap : IEntityTypeConfiguration<Comuna>
    {
        public void Configure(EntityTypeBuilder<Comuna> builder)
        {
            builder.ToTable(Database.Tables.comuna.TableName, Database.Schema);
            builder.HasKey(x => x.IdComuna);
            builder.Property(x => x.IdComuna)
                .HasColumnName(Database.Tables.comuna.Columns.IdComuna)
            .HasColumnType(Database.Types.Int);
        builder.Property(x => x.Nombre)
            .HasColumnName(Database.Tables.comuna.Columns.Nombre)
            .HasColumnType(Database.Types.Varchar2)
            .HasMaxLength(255);

    }
}
}
