﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class EmpresaMap : IEntityTypeConfiguration<Empresa>
    {
        public void Configure(EntityTypeBuilder<Empresa> builder)
        {
            builder.ToTable(Database.Tables.empresa.TableName, Database.Schema);
            builder.HasKey(x => x.IdEmpresa);
            builder.Property(x => x.IdEmpresa)
                .HasColumnName(Database.Tables.empresa.Columns.IdEmpresa)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Nombre)
                .HasColumnName(Database.Tables.empresa.Columns.Nombre)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.RazonSocial)
                .HasColumnName(Database.Tables.empresa.Columns.RazonSocial)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.FechaIngreso)
                .HasColumnName(Database.Tables.empresa.Columns.FechaIngreso)
                .HasColumnType(Database.Types.Date);
            builder.Property(x => x.Direccion)
                .HasColumnName(Database.Tables.empresa.Columns.Direccion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Cargo)
                .HasColumnName(Database.Tables.empresa.Columns.Cargo)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Profesion)
                .HasColumnName(Database.Tables.empresa.Columns.Profesion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.IdCajaCompensacion)
                .HasColumnName(Database.Tables.empresa.Columns.IdCajaCompensacion)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.NombreContacto)
                .HasColumnName(Database.Tables.empresa.Columns.NombreContacto)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.ApellidoPaterno)
                .HasColumnName(Database.Tables.empresa.Columns.ApellidoPaterno)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.ApellidoMaterno)
                .HasColumnName(Database.Tables.empresa.Columns.ApellidoMaterno)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Email)
                .HasColumnName(Database.Tables.empresa.Columns.Email)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Telefono)
                .HasColumnName(Database.Tables.empresa.Columns.Telefono)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Rut)
                .HasColumnName(Database.Tables.empresa.Columns.Rut)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Comuna)
                .HasColumnName(Database.Tables.empresa.Columns.Comuna)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdUsuario)
                .HasColumnName(Database.Tables.empresa.Columns.IdUsuario)
                .HasColumnType(Database.Types.Int);
        }
    }
}
