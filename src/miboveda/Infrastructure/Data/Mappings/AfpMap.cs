﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace miboveda.Infrastructure.Data.Mappings
{
    public class AfpMap : IEntityTypeConfiguration<afp>
    {
        public void Configure(EntityTypeBuilder<afp> builder)
        {
            builder.ToTable(Database.Tables.afp.TableName, Database.Schema);
            builder.HasKey(x => x.IdAfp);
            builder.Property(x => x.IdAfp)
                .HasColumnName(Database.Tables.afp.Columns.IdAfp)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Nombre)
                .HasColumnName(Database.Tables.afp.Columns.Nombre)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
