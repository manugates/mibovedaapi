﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class PermisoMap : IEntityTypeConfiguration<Permiso>
    {
        public void Configure(EntityTypeBuilder<Permiso> builder)
        {
            builder.ToTable(Database.Tables.permiso.TableName, Database.Schema);
            builder.HasKey(x => x.IdContacto);
            builder.Property(x => x.Seguro)
                .HasColumnName(Database.Tables.permiso.Columns.Seguro)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.CuentaBancaria)
                .HasColumnName(Database.Tables.permiso.Columns.CuentaBancaria)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.DatosProvisionales)
                .HasColumnName(Database.Tables.permiso.Columns.DatosPrevisionales)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Bienes)
                .HasColumnName(Database.Tables.permiso.Columns.Bienes)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Deudas)
                .HasColumnName(Database.Tables.permiso.Columns.Deudas)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.FotosVideos)
                .HasColumnName(Database.Tables.permiso.Columns.FotosVideos)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Documentos)
                .HasColumnName(Database.Tables.permiso.Columns.Documentos)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Deseos)
                .HasColumnName(Database.Tables.permiso.Columns.Deseos)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Instrucciones)
                .HasColumnName(Database.Tables.permiso.Columns.Instrucciones)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Testimonios)
                .HasColumnName(Database.Tables.permiso.Columns.Testimonios)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdContacto)
                .HasColumnName(Database.Tables.permiso.Columns.IdContacto)
                .HasColumnType(Database.Types.Int);
             


        }
    }
}
