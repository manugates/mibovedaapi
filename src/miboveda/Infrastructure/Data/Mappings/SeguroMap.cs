﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class SeguroMap : IEntityTypeConfiguration<Seguro>
    {
        public void Configure(EntityTypeBuilder<Seguro> builder)
        {
            builder.ToTable(Database.Tables.seguro.TableName, Database.Schema);
            builder.HasKey(x => x.IdSeguro);
            builder.Property(x => x.IdSeguro)
                .HasColumnName(Database.Tables.seguro.Columns.IdSeguro)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.NumeroPoliza)
                .HasColumnName(Database.Tables.seguro.Columns.NumeroPoliza)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.IdTipoSeguro)
                .HasColumnName(Database.Tables.seguro.Columns.IdTipoSeguro)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdAseguradora)
                .HasColumnName(Database.Tables.seguro.Columns.IdAseguradora)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdUsuario)
                .HasColumnName(Database.Tables.seguro.Columns.IdUsuario)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Vigencia)
                .HasColumnName(Database.Tables.seguro.Columns.Vigencia)
                .HasColumnType(Database.Types.Date);
            builder.Property(x => x.Ahorro)
                .HasColumnName(Database.Tables.seguro.Columns.Ahorro)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
