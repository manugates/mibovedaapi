﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class CasaComercialMap : IEntityTypeConfiguration<CasaComercial>
    {
        public void Configure(EntityTypeBuilder<CasaComercial> builder)
        {
            builder.ToTable(Database.Tables.casaComercial.TableName, Database.Schema);
            builder.HasKey(x => x.IdCasaComercial);
            builder.Property(x => x.IdCasaComercial)
                .HasColumnName(Database.Tables.casaComercial.Columns.idCasaComercial)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.casaComercial.Columns.descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
