﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace miboveda.Infrastructure.Data.Mappings
{
    public class TipoCuentaMap : IEntityTypeConfiguration<TipoCuenta>
    {
        public void Configure(EntityTypeBuilder<TipoCuenta> builder)
        {
            builder.ToTable(Database.Tables.tipoCuenta.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoCuenta);
            builder.Property(x => x.IdTipoCuenta)
                .HasColumnName(Database.Tables.tipoCuenta.Columns.IdTipoCuenta)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoCuenta.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
