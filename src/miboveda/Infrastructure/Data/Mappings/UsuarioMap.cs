﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable(Database.Tables.usuario.TableName, Database.Schema);
            builder.HasKey(x => x.IdUsuario);
            builder.Property(x => x.IdUsuario)
                .HasColumnName(Database.Tables.usuario.Columns.IdUsuario)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Rut)
                .HasColumnName(Database.Tables.usuario.Columns.Rut)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Password)
                .HasColumnName(Database.Tables.usuario.Columns.Password)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Email)
                .HasColumnName(Database.Tables.usuario.Columns.Email)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.FechaNacimiento)
                .HasColumnName(Database.Tables.usuario.Columns.FechaNacimiento)
                .HasColumnType(Database.Types.Date);
            builder.Property(x => x.FechaInscripcion)
                .HasColumnName(Database.Tables.usuario.Columns.FechaInscripcion)
                .HasColumnType(Database.Types.Date);
            builder.Property(x => x.Nombre)
                .HasColumnName(Database.Tables.usuario.Columns.Nombre)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.ApellidoPaterno)
                .HasColumnName(Database.Tables.usuario.Columns.ApellidoPaterno)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.ApellidoMaterno)
                .HasColumnName(Database.Tables.usuario.Columns.ApellidoMaterno)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.FechaCreacion)
                .HasColumnName(Database.Tables.usuario.Columns.FechaCreacion)
                .HasColumnType(Database.Types.Date);
            builder.Property(x => x.FechaUpdate)
                .HasColumnName(Database.Tables.usuario.Columns.FechaUpdate)
                .HasColumnType(Database.Types.Date);
            builder.Property(x => x.CreadoPor)
                .HasColumnName(Database.Tables.usuario.Columns.CreadoPor)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdAfp)
                .HasColumnName(Database.Tables.usuario.Columns.IdAfp)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdIsapre)
                .HasColumnName(Database.Tables.usuario.Columns.IdIsapre)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdPlan)
                .HasColumnName(Database.Tables.usuario.Columns.IdPlan)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Telefono)
                .HasColumnName(Database.Tables.usuario.Columns.Telefono)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Comuna)
                .HasColumnName(Database.Tables.usuario.Columns.Comuna)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
        }
    }
}
