﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class TipoFondoMap : IEntityTypeConfiguration<TipoFondo>
    {
        public void Configure(EntityTypeBuilder<TipoFondo> builder)
        {
            builder.ToTable(Database.Tables.tipoFondo.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoFondo);
            builder.Property(x => x.IdTipoFondo)
                .HasColumnName(Database.Tables.tipoFondo.Columns.IdTipoFondo)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoFondo.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
