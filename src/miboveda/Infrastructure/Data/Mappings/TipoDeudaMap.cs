﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class TipoDeudaMap : IEntityTypeConfiguration<TipoDeuda>
    {
        public void Configure(EntityTypeBuilder<TipoDeuda> builder)
        {
            builder.ToTable(Database.Tables.tipoDeuda.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoDeuda);
            builder.Property(x => x.IdTipoDeuda)
                .HasColumnName(Database.Tables.tipoDeuda.Columns.IdTipoDeuda)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoDeuda.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
