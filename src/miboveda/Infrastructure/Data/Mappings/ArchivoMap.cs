﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class ArchivoMap : IEntityTypeConfiguration<Archivo>
    {
        public void Configure(EntityTypeBuilder<Archivo> builder)
        {
            builder.ToTable(Database.Tables.archivo.TableName, Database.Schema);
            builder.HasKey(x => x.IdArchivo);
            builder.Property(x => x.IdArchivo)
                .HasColumnName(Database.Tables.archivo.Columns.IdArchivo)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.File)
                .HasColumnName(Database.Tables.archivo.Columns.File)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdUsuario)
                .HasColumnName(Database.Tables.archivo.Columns.IdUsuario)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdTipoArchivo)
                .HasColumnName(Database.Tables.archivo.Columns.IdTipoArchivo)
                .HasColumnType(Database.Types.Int);

        }
    }
}
