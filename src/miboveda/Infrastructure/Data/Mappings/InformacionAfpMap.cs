﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class InformacionAfpMap : IEntityTypeConfiguration<InformacionAfp>
    {
        public void Configure(EntityTypeBuilder<InformacionAfp> builder)
        {
            builder.ToTable(Database.Tables.informacionAfp.TableName, Database.Schema);
            builder.HasKey(x => x.IdInformacionAfp);
            builder.Property(x => x.IdInformacionAfp)
                .HasColumnName(Database.Tables.informacionAfp.Columns.IdInformacionAfp)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdTipoFondo)
                .HasColumnName(Database.Tables.informacionAfp.Columns.IdTipoFondo)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdUsuario)
                .HasColumnName(Database.Tables.informacionAfp.Columns.IdUsuario)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdAfp)
                .HasColumnName(Database.Tables.informacionAfp.Columns.IdAfp)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdTipoCotizacion)
                .HasColumnName(Database.Tables.informacionAfp.Columns.IdTipoCotizacion)
                .HasColumnType(Database.Types.Int);


        }
    }
}
