﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class ParentescoMap : IEntityTypeConfiguration<Parentesco>
    {
        public void Configure(EntityTypeBuilder<Parentesco> builder)
        {
            builder.ToTable(Database.Tables.parentesco.TableName, Database.Schema);
            builder.HasKey(x => x.IdParentesco);
            builder.Property(x => x.IdParentesco)
                .HasColumnName(Database.Tables.parentesco.Columns.IdParentesco)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.parentesco.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
