﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace miboveda.Infrastructure.Data.Mappings
{
    public class AseguradoraMap : IEntityTypeConfiguration<Aseguradora>
    {
        public void Configure(EntityTypeBuilder<Aseguradora> builder)
        {
            builder.ToTable(Database.Tables.aseguradora.TableName, Database.Schema);
            builder.HasKey(x => x.IdAseguradora);
            builder.Property(x => x.IdAseguradora)
                .HasColumnName(Database.Tables.aseguradora.Columns.IdAseguradora)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.aseguradora.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
