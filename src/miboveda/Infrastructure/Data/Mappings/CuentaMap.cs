﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class CuentaMap : IEntityTypeConfiguration<Cuenta>
    {
        public void Configure(EntityTypeBuilder<Cuenta> builder)
        {
            builder.ToTable(Database.Tables.cuenta.TableName, Database.Schema);
            builder.HasKey(x => x.IdCuenta);
            builder.Property(x => x.IdCuenta)
                .HasColumnName(Database.Tables.cuenta.Columns.IdCuenta)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Numero)
                .HasColumnName(Database.Tables.cuenta.Columns.Numero)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.IdBanco)
                .HasColumnName(Database.Tables.cuenta.Columns.IdBanco)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdTipoCuenta)
                .HasColumnName(Database.Tables.cuenta.Columns.IdTipoCuenta)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdUsuario)
                .HasColumnName(Database.Tables.cuenta.Columns.IdUsuario)
                .HasColumnType(Database.Types.Int);

        }
    }
}
