﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class BeneficiarioMap : IEntityTypeConfiguration<Beneficiario>
    {
        public void Configure(EntityTypeBuilder<Beneficiario> builder)
        {
            builder.ToTable(Database.Tables.beneficiario.TableName, Database.Schema);
            builder.HasKey(x => x.IdBeneficiario);
            builder.Property(x => x.IdBeneficiario)
                .HasColumnName(Database.Tables.beneficiario.Columns.IdBeneficiario)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Nombre)
                .HasColumnName(Database.Tables.beneficiario.Columns.Nombre)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.ApellidoPaterno)
                .HasColumnName(Database.Tables.beneficiario.Columns.ApellidoPaterno)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.ApellidoMaterno)
                .HasColumnName(Database.Tables.beneficiario.Columns.ApellidoMaterno)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Rut)
                .HasColumnName(Database.Tables.beneficiario.Columns.Rut)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.FechaNacimiento)
                .HasColumnName(Database.Tables.beneficiario.Columns.FechaNacimiento)
                .HasColumnType(Database.Types.Date);
            builder.Property(x => x.Email)
                .HasColumnName(Database.Tables.beneficiario.Columns.Email)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Telefono)
                .HasColumnName(Database.Tables.beneficiario.Columns.Telefono)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.IdSeguro)
                .HasColumnName(Database.Tables.beneficiario.Columns.IdSeguro)
                .HasColumnType(Database.Types.Int);
        }
    }
}
