﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class TipoPlanSaludMap : IEntityTypeConfiguration<TipoPlanSalud>
    {
        public void Configure(EntityTypeBuilder<TipoPlanSalud> builder)
        {
            builder.ToTable(Database.Tables.tipoPlanSalud.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoPlan);
            builder.Property(x => x.IdTipoPlan)
                .HasColumnName(Database.Tables.tipoPlanSalud.Columns.IdTipoPlan)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoPlanSalud.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
