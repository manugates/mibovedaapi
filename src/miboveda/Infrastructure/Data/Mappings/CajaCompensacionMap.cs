﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace miboveda.Infrastructure.Data.Mappings
{
    public class CajaCompensacionMap : IEntityTypeConfiguration<CajaCompensacion>
    {
        public void Configure(EntityTypeBuilder<CajaCompensacion> builder)
        {
            builder.ToTable(Database.Tables.cajaCompensacion.TableName, Database.Schema);
            builder.HasKey(x => x.IdCaja);
            builder.Property(x => x.IdCaja)
                .HasColumnName(Database.Tables.cajaCompensacion.Columns.IdCaja)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Nombre)
                .HasColumnName(Database.Tables.cajaCompensacion.Columns.Nombre)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
