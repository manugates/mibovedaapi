﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class TipoCotizacionMap : IEntityTypeConfiguration<TipoCotizacion>
    {
        public void Configure(EntityTypeBuilder<TipoCotizacion> builder)
        {
            builder.ToTable(Database.Tables.tipoCotizacion.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoCotizacion);
            builder.Property(x => x.IdTipoCotizacion)
                .HasColumnName(Database.Tables.tipoCotizacion.Columns.IdTipoCotizacion)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoCotizacion.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
