﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class TipoArchivoMap : IEntityTypeConfiguration<TipoArchivo>
    {
        public void Configure(EntityTypeBuilder<TipoArchivo> builder)
        {
            builder.ToTable(Database.Tables.tipoArchivo.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoArchivo);
            builder.Property(x => x.IdTipoArchivo)
                .HasColumnName(Database.Tables.tipoArchivo.Columns.IdTipoArchivo)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoArchivo.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
