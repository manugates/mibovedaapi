﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class ContacoMap : IEntityTypeConfiguration<Contacto>
    {
        public void Configure(EntityTypeBuilder<Contacto> builder)
        {
            builder.ToTable(Database.Tables.contacto.TableName, Database.Schema);
            builder.HasKey(x => x.IdContacto);
            builder.Property(x => x.IdContacto)
                .HasColumnName(Database.Tables.contacto.Columns.IdContacto)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Nombre)
                .HasColumnName(Database.Tables.contacto.Columns.Nombre)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.ApellidoPaterno)
                .HasColumnName(Database.Tables.contacto.Columns.ApellidoPaterno)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.ApellidoMaterno)
                .HasColumnName(Database.Tables.contacto.Columns.ApellidoMaterno)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.FechaNacimiento)
                .HasColumnName(Database.Tables.contacto.Columns.FechaNacimiento)
                .HasColumnType(Database.Types.Date);
            builder.Property(x => x.Rut)
                .HasColumnName(Database.Tables.contacto.Columns.Rut)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Password)
                .HasColumnName(Database.Tables.contacto.Columns.Password)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.FechaInscripcion)
                .HasColumnName(Database.Tables.contacto.Columns.FechaInscripcion)
                .HasColumnType(Database.Types.Date);
            builder.Property(x => x.Email)
                .HasColumnName(Database.Tables.contacto.Columns.Email)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.IdUsuario)
                .HasColumnName(Database.Tables.contacto.Columns.IdUsuario)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdParentesco)
                .HasColumnName(Database.Tables.contacto.Columns.IdParentesco)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Sexo)
                .HasColumnName(Database.Tables.contacto.Columns.Sexo)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
