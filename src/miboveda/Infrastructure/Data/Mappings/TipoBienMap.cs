﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class TipoBienMap : IEntityTypeConfiguration<TipoBien>
    {
        public void Configure(EntityTypeBuilder<TipoBien> builder)
        {
            builder.ToTable(Database.Tables.tipoBien.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoBien);
            builder.Property(x => x.IdTipoBien)
                .HasColumnName(Database.Tables.tipoBien.Columns.IdTipoBien)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoBien.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
