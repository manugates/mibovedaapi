﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class TipoMuebleMap : IEntityTypeConfiguration<TipoMueble>
    {
        public void Configure(EntityTypeBuilder<TipoMueble> builder)
        {
            builder.ToTable(Database.Tables.tipoMueble.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoMueble);
            builder.Property(x => x.IdTipoMueble)
                .HasColumnName(Database.Tables.tipoMueble.Columns.IdTipoMueble)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoMueble.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
        }
    }
}
