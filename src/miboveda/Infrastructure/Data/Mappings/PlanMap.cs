﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class PlanMap : IEntityTypeConfiguration<Plan>
    {
        public void Configure(EntityTypeBuilder<Plan> builder)
        {
            builder.ToTable(Database.Tables.plan.TableName, Database.Schema);
            builder.HasKey(x => x.IdPlan);
            builder.Property(x => x.IdPlan)
                .HasColumnName(Database.Tables.plan.Columns.IdPlan)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.plan.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Precio)
                .HasColumnName(Database.Tables.plan.Columns.Precio)
                .HasColumnType(Database.Types.Int);
        }
    }
}
