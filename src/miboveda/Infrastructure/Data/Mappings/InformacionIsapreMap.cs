﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class InformacionIsapreMap : IEntityTypeConfiguration<InformacionIsapre>
    {
        public void Configure(EntityTypeBuilder<InformacionIsapre> builder)
        {
            builder.ToTable(Database.Tables.informacionIsapre.TableName, Database.Schema);
            builder.HasKey(x => x.IdInformacionIsapre);
            builder.Property(x => x.IdInformacionIsapre)
                .HasColumnName(Database.Tables.informacionIsapre.Columns.IdInformacionIsapre)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdTipoIsapre)
                .HasColumnName(Database.Tables.informacionIsapre.Columns.IdTIpoIsapre)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdUsuario)
                .HasColumnName(Database.Tables.informacionIsapre.Columns.IdUsuario)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdIsapre)
                .HasColumnName(Database.Tables.informacionIsapre.Columns.IdIsapre)
                .HasColumnType(Database.Types.Int);

        }
    }
}
