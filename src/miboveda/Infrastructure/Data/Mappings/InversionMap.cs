﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class InversionMap : IEntityTypeConfiguration<Inversion>
    {
        public void Configure(EntityTypeBuilder<Inversion> builder)
        {
            builder.ToTable(Database.Tables.inversion.TableName, Database.Schema);
            builder.HasKey(x => x.IdInversion);
            builder.Property(x => x.IdInversion)
                .HasColumnName(Database.Tables.inversion.Columns.IdInversion)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.inversion.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.IdTipoInversion)
                .HasColumnName(Database.Tables.inversion.Columns.IdTipoInversion)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdBanco)
               .HasColumnName(Database.Tables.inversion.Columns.IdBanco)
               .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdUsuario)
               .HasColumnName(Database.Tables.inversion.Columns.IdUsuario)
               .HasColumnType(Database.Types.Int);

        }
    }
}
