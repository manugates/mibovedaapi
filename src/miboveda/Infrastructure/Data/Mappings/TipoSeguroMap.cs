﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings 
{
    public class TipoSeguroMap : IEntityTypeConfiguration<TipoSeguro>
    {
        public void Configure(EntityTypeBuilder<TipoSeguro> builder)
        {
            builder.ToTable(Database.Tables.tipoSeguro.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoSeguro);
            builder.Property(x => x.IdTipoSeguro)
                .HasColumnName(Database.Tables.tipoSeguro.Columns.IdTipoSeguro)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoSeguro.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
