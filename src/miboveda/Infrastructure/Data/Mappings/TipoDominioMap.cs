﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class TipoDominioMap : IEntityTypeConfiguration<TipoDominio>
    {
        public void Configure(EntityTypeBuilder<TipoDominio> builder)
        {
            builder.ToTable(Database.Tables.tipoDominio.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoDominio);
            builder.Property(x => x.IdTipoDominio)
                .HasColumnName(Database.Tables.tipoDominio.Columns.IdTipoDominio)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoDominio.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
