﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class TipoInmuebleMap : IEntityTypeConfiguration<TipoInmueble>
    {
        public void Configure(EntityTypeBuilder<TipoInmueble> builder)
        {
            builder.ToTable(Database.Tables.tipoInmueble.TableName, Database.Schema);
            builder.HasKey(x => x.IdTipoInmueble);
            builder.Property(x => x.IdTipoInmueble)
                .HasColumnName(Database.Tables.tipoInmueble.Columns.IdTipoInmueble)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Descripcion)
                .HasColumnName(Database.Tables.tipoInmueble.Columns.Descripcion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
