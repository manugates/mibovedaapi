﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class DeudaMap : IEntityTypeConfiguration<Deuda>
    {
        public void Configure(EntityTypeBuilder<Deuda> builder)
        {
            builder.ToTable(Database.Tables.deuda.TableName, Database.Schema);
            builder.HasKey(x => x.IdDeuda);
            builder.Property(x => x.IdDeuda)
                .HasColumnName(Database.Tables.deuda.Columns.IdDeuda)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Operacion)
                .HasColumnName(Database.Tables.deuda.Columns.Operacion)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Monto)
                .HasColumnName(Database.Tables.deuda.Columns.Monto)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.SeguroDegravament)
                .HasColumnName(Database.Tables.deuda.Columns.SeguroDegravament)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.Mora)
                .HasColumnName(Database.Tables.deuda.Columns.Mora)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);
            builder.Property(x => x.IdUsuario)
                .HasColumnName(Database.Tables.deuda.Columns.IdUsuario)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdTipoDeuda)
                .HasColumnName(Database.Tables.deuda.Columns.IdTipoDeuda)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdBanco)
                .HasColumnName(Database.Tables.deuda.Columns.IdBanco)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdProducto)
                .HasColumnName(Database.Tables.deuda.Columns.IdProducto)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdCaja)
                .HasColumnName(Database.Tables.deuda.Columns.IdCaja)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdCasaComercial)
                .HasColumnName(Database.Tables.deuda.Columns.IdCasaComercial)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.IdEntidad)
                .HasColumnName(Database.Tables.deuda.Columns.IdEntidad)
                .HasColumnType(Database.Types.Int);

        }
    }
}
