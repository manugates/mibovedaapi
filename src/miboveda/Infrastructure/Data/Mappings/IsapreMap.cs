﻿using miboveda.Entities;
using miboveda.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace miboveda.Infrastructure.Data.Mappings
{
    public class IsapreMap : IEntityTypeConfiguration<Isapre>
    {
        public void Configure(EntityTypeBuilder<Isapre> builder)
        {
            builder.ToTable(Database.Tables.isapre.TableName, Database.Schema);
            builder.HasKey(x => x.IdIsapre);
            builder.Property(x => x.IdIsapre)
                .HasColumnName(Database.Tables.isapre.Columns.IdIsapre)
                .HasColumnType(Database.Types.Int);
            builder.Property(x => x.Nombre)
                .HasColumnName(Database.Tables.isapre.Columns.Nombre)
                .HasColumnType(Database.Types.Varchar2)
                .HasMaxLength(255);

        }
    }
}
