﻿using System.Threading.Tasks;
using miboveda.Entities;
using miboveda.Infrastructure.Data.Mappings;
using Microsoft.EntityFrameworkCore;


namespace miboveda.Infrastructure.Data
{
    public class MiBovedaDbContext : DbContext
    {
        public MiBovedaDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<afp> Afps { get; set; }
        public DbSet<Isapre> Isapres { get; set; }
        public DbSet<TipoDeuda> TipoDeudas { get; set; }
        public DbSet<TipoArchivo> TipoArchivos { get; set; }
        public DbSet<TipoBien> TipoBienes { get; set; }
        public DbSet<TipoCuenta> TipoCuentas { get; set; }
        public DbSet<TipoInversion> TipoInversiones { get; set; }
        public DbSet<Parentesco> Parentescos { get; set; }
        public DbSet<TipoSeguro> TipoSeguros { get; set; }
        public DbSet<CajaCompensacion> CajaCompensaciones { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Seguro> Seguros { get; set; }
        public DbSet<Plan> Planes { get; set; }
        public DbSet<Permiso> Permisos { get; set; }
        public DbSet<Inversion> Inversiones { get; set; }
        public DbSet<Aseguradora> Aseguradoras { get; set; }
        public DbSet<Archivo> Archivos { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Deuda> Deudas { get; set; }
        public DbSet<Cuenta> Cuentas { get; set; }
        public DbSet<Contacto> Contactos { get; set; }
        public DbSet<Bienes> Bienes { get; set; }
        public DbSet<Banco> Bancos { get; set; }
        public DbSet<Secuencia> Secuencias { get; set; }
        public DbSet<Comuna> Comunas { get; set; }
        public DbSet<Beneficiario> Beneficiarios { get; set; }
        public DbSet<TipoPlanSalud> TiposPlanSalud { get; set; }
        public DbSet<TipoFondo> TipoFondos { get; set; }
        public DbSet<TipoCotizacion> TipoCotizaciones { get; set; }
        public DbSet<InformacionAfp> InformacionAfps { get; set; }
        public DbSet<InformacionIsapre> InformacionIsapres { get; set; }
        public DbSet<InformacionCaja> InformacionCajas { get; set; }
        public DbSet<TipoDominio> TipoDominios { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<CasaComercial> CasasComerciales { get; set; }
        public DbSet<Entidad> Entidades { get; set; }
        public DbSet<TipoInmueble> TipoInmuebles { get; set; }
        public DbSet<TipoMueble> TipoMuebles { get; set; }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.ApplyConfiguration(new AfpMap());
            modelBuilder.ApplyConfiguration(new IsapreMap());
            modelBuilder.ApplyConfiguration(new TipoDeudaMap());
            modelBuilder.ApplyConfiguration(new TipoArchivoMap());
            modelBuilder.ApplyConfiguration(new TipoBienMap());
            modelBuilder.ApplyConfiguration(new TipoCuentaMap());
            modelBuilder.ApplyConfiguration(new TipoInversionMap());
            modelBuilder.ApplyConfiguration(new ParentescoMap());
            modelBuilder.ApplyConfiguration(new TipoSeguroMap());
            modelBuilder.ApplyConfiguration(new CajaCompensacionMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new SeguroMap());
            modelBuilder.ApplyConfiguration(new PlanMap());
            modelBuilder.ApplyConfiguration(new PermisoMap());
            modelBuilder.ApplyConfiguration(new InversionMap());
            modelBuilder.ApplyConfiguration(new AseguradoraMap());
            modelBuilder.ApplyConfiguration(new ArchivoMap());
            modelBuilder.ApplyConfiguration(new EmpresaMap());
            modelBuilder.ApplyConfiguration(new DeudaMap());
            modelBuilder.ApplyConfiguration(new CuentaMap());
            modelBuilder.ApplyConfiguration(new ContacoMap());
            modelBuilder.ApplyConfiguration(new BienesMap());
            modelBuilder.ApplyConfiguration(new BancoMap());
            modelBuilder.ApplyConfiguration(new SecuenciaMap());
            modelBuilder.ApplyConfiguration(new ComunaMap());
            modelBuilder.ApplyConfiguration(new BeneficiarioMap());
            modelBuilder.ApplyConfiguration(new TipoPlanSaludMap());
            modelBuilder.ApplyConfiguration(new TipoFondoMap());
            modelBuilder.ApplyConfiguration(new TipoCotizacionMap());
            modelBuilder.ApplyConfiguration(new InformacionAfpMap());
            modelBuilder.ApplyConfiguration(new InformacionIsapreMap());
            modelBuilder.ApplyConfiguration(new InformacionCajaCompensacionMap());
            modelBuilder.ApplyConfiguration(new TipoDominioMap());
            modelBuilder.ApplyConfiguration(new ProductoMap());
            modelBuilder.ApplyConfiguration(new CasaComercialMap());
            modelBuilder.ApplyConfiguration(new EntidadMap());
            modelBuilder.ApplyConfiguration(new TipoInmuebleMap());
            modelBuilder.ApplyConfiguration(new TipoMuebleMap());
        }

    }
}
